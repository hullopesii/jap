<?PHP
	session_start();
  if (empty($_SESSION['iduser'])){
	  $_SESSION['iduser'] = 0;
	   header("Location:index.php");
	}
?>
 <?PHP
	
	
	$id = $_GET['id'];
	
	require_once("../classes/Funcoes.php");
	$funcoes = new Funcoes();
	
	require_once("../classes/Connection.php");
	$query = new Connection();
	
	$resp = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE `idCotacao`={$id}");
	$row = $resp->fetch_assoc();
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script src="../scripts/jquery.1.5.2.min.js"></script>
    <script src="../scripts/jquery.tablesorter.min.js"></script>
    <script src="../scripts/jquery.tablesorter.pager.js"></script>
	<script type="text/javascript">
	      function submita(){
			  //$("#pedido").submit();
			  document.getElementById("pedido").submit();
		   }
				   
	</script>
<style>
  .porta1 a{width:200px; height:100px; float:left; background:#CCCCCC; margin:5px; padding:2px; text-align:center;}
  table{
  width:98%;
  border:1px solid #ccc;
  font-size:12px;
}

table thead th{
  background-color:#ad2525;
  padding:6px;
  color:#fff;
  text-align:center;
  font-size:12px;
}

table thead th.header{
  cursor:pointer;
}

table tbody td{
  padding:6px;
  text-align:center;
  color:#333;
}

table tbody tr.odd td{
  background-color:#ffffcc;
}

table tbody tr.hover td{
  background-color:#a9d0f5;
}

table tbody tr.selected td{
  background-color:#a9f5a9!important;
}

.pagedisplay{
	width:40px;
	text-align:center;
	border:none;
	background-color:#f2f2f2;
	font-weight:900;
}

#pager span{
	font-weight:900;
	display:inline-block;
	margin:0 0 0 20px;
	color:#666;
	float:right;
}

#pager form{
	text-align:left;
	padding:10px;
	width:580px;
}
a img{
	border:none;
}

form{
  background-color:#f2f2f2;
  width:600px;
  margin:10px 0 10px 0;
  text-align:center;
}

form p{
  padding:12px;
}

form p label{
  color:#333;
  font-weight:900;
}

form input{
  padding:6px;
  border:1px solid #ccc;
  width:300px;
}
</style>
</head>

<body>
<h1>Avisando os fornecedores - Cotação código <?php echo $row['idCotacao']." data ". $funcoes->trataData($row['data']); ?></h1>
  
  <table width="100%" border="0" cellspacing="3" cellpadding="3">
    <thead>
	  <tr>
		<td width="5%" bgcolor="#CCCCCC">C&oacute;digo</td>
		<td width="60%" bgcolor="#CCCCCC">Fornecedor</td>
		<td width="15%" bgcolor="#CCCCCC">Fone</td>
		<td width="15%" bgcolor="#CCCCCC">E-mail</td>
		<td width="5%" bgcolor="#CCCCCC">Avisar<br /><!--<a href="#" id="marcar-todos" title="Desmarcar todos">Desmarcar Todos</a>--><input type="checkbox" value="1" id="marcar-todos" name="marcar-todos" title="Marcar/desmarcar todos" /></td>
	  </tr>
	 </thead> 
		  <form name="pedido" id="pedido" action="avisaFornecedores.php?id=<?php echo $id; ?>" method="post">
		  <tbody>
		  <?php
			  
			  //buscando os fornecedores
	  		  $res = $query->getConsulta2("SELECT * FROM `fornecedores` order by `fornecedor`");

			  while ($r = $res->fetch_assoc()){
				 
				 $input = '<input name="'.$r['idFornecedor'].'" type="checkbox" value="'.$r['idFornecedor'].'" checked />';

				 echo ' <tr>
							<td>'.$r['idFornecedor'].'</td>
							<td  style="text-align:left">'.$r['fornecedor'].'</td>
							<td>'.$r['fone'].'</td>
							<td>'.$r['email'].'</td>
							<td>';
				 echo $input;
				 echo '			
							</td>
						  </tr>';
			  }
			  //echo $html;
		  ?>
		   </tbody>
		 </form>  
	</table>
	 <input type="button" value="Enviar Cotação" style=" width:200px; height:40px; float:right;" onclick="submita()" />


<script>
    $(function(){
      
      $('table > tbody > tr:odd').addClass('odd');
      
      $('table > tbody > tr').hover(function(){
        $(this).toggleClass('hover');
      });
      
      /*$('#marcar-todos').click(function(){
        $('table > tbody > tr > td > :checkbox')
          .attr('checked', $(this).is(':checked'))
          .trigger('change');
      });*/
      
      $('#marcar-todos').click(function(){
      		$('table > tbody > tr > td > :checkbox').attr('checked', $(this).is(':checked')).trigger('change');
      });
      
      
      $('table > tbody > tr > td > :checkbox').bind('click change', function(){
        var tr = $(this).parent().parent();
        if($(this).is(':checked')) $(tr).addClass('selected');
        else $(tr).removeClass('selected');
      });
      
      $('form').submit(function(e){ e.preventDefault(); });
      
      $('#pesquisar').keydown(function(){
        var encontrou = false;
        var termo = $(this).val().toLowerCase();
        $('table > tbody > tr').each(function(){
          $(this).find('td').each(function(){
            if($(this).text().toLowerCase().indexOf(termo) > -1) encontrou = true;
          });
          if(!encontrou) $(this).hide();
          else $(this).show();
          encontrou = false;
        });
      });
      
      
      
    });
    </script>

</body>
</html>