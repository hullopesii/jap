<?PHP
	// para controle de segurança, utilizaremos variáveis de sessão, isto aqui está inicializando a sessão
	session_start();
	require_once("../classes/Connection.php");
	
	$query = new Connection();
	
	
	require_once('../classes/Funcoes.php');
	$funcoes = new Funcoes();
	
	if (isset($_GET['id'])){	
	 		$id = $_GET['id'];
			$r = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE `idCotacao`=".$id);
			
	}else{
	   header("location:cotacoes.php");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gestão de popups - janelas de mensagens</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
<script type="text/javascript" src="script/prototype.js"></script>
<?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
if ($_SESSION['iduser'] > 0){
?>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
  dojo.require("dijit.form.Button");
  dojo.require("dijit.form.TextBox");
  dojo.require("dojox.form.DropDownSelect");
  dojo.require("dojox.grid.DataGrid");
  dojo.require("dijit.form.DateTextBox");
  dojo.require("dojo.data.ItemFileWriteStore");
  dojo.require("dijit.Menu");
   
  idUserLogado = "<?php echo $_SESSION['iduser'] ?>";

  var idCotacao = 0;
  var grid = null; 
  dojo.addOnLoad(function(){	
		
		 var jsonStore = new dojo.data.ItemFileWriteStore({ url: "json/itensCotacoes.php?id=+<?php echo $id; ?>" , clearOnClose: true});  
		  
	     var layout= [	
		 		{ field: "part_num", width: "40px", name: "id", editable:false },
				{ field: "codigo", width: "100px", name: "Código", editable: true },  
				{ field: "descricao", width: "550px", name: "Descrição", editable: true }, 
				{ field: "referencia", width: "200px", name: "Marca", editable: true },
				{ field: "qtdCotada", width: "100px", name: "Qtd Cotada", editable: true }
				];
		 
		grid = new dojox.grid.DataGrid({
						Id: "grid1",
						singleClickEdit: false,
						store: jsonStore,
						structure: layout,
						loadingMessage:'Carregando itens',
						onApplyCellEdit: editEduRow,
						rowsPerPage: 20	},
						 'gridNode');
		 grid.store.close();
		 //grid.onApplyEdit(editando);
		grid.startup();
		dojo.connect(grid, "onKeyPress", clique);
		grid.onRowContextMenu = function(e){
			   
				var rowValue = grid.getItem(e.rowIndex).part_num;
				idCotacao = rowValue;	
		}
		
	});
	
	function clique(tecla){   			
	  if(tecla.keyCode == 46){    //se a tecla foi o del, vamos deletar o item selecionado
       if (confirm('Deseja realmente excluir esta popup?')){
		 var identificador = pegandoId(grid.selection.getSelected());
		 
			 var url = 'excluindoItemCotacao.php?id='+identificador+'&iduser='+idUserLogado;
			 
			 retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
		     
		}//fecha confirm 
		 
	  }
	}
	
	function pegandoId(linhaSelecionada){
	  var arrayform = new Array();
	  if(linhaSelecionada.length){
            dojo.forEach(linhaSelecionada, function(selectedItem) {
                if(selectedItem !== null) {
					var ii = 0;					
                    dojo.forEach(grid.store.getAttributes(selectedItem), function(attribute) {
                        var value = grid.store.getValues(selectedItem, attribute);
                        
						arrayform[ii] = value;
						ii++; 
                    }); // end forEach
                } // end if
            }); // end forEach
        } // end if
		
		return arrayform[0];
	}
	
	function editEduRow(txt, index, campo){
		
		var identificador = pegandoId(grid.selection.getSelected());

		var url = 'alterandoItensCotacoes.php?id='+identificador+'&campo='+campo+'&txt='+txt;//variavel que vai mandar os dados via get 
		retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
	 
	}
	
	function carregando(){
		$('loader').style.visibility='visible';	
		$('modal').style.visibility='visible';
	}
	function carregado(){
		$('loader').style.visibility='hidden';	
		$('modal').style.visibility='hidden';
	}
	function falha(){
	  alert('falha no carregamento');
	  carregado();
	}
	function resposta(resp){
	    carregado();
		var json = resp.responseText;//pegando o texto retornado pela página de alteração
		if (json == 'ok'){
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/itensCotacoes.php?id=+<?php echo $id; ?>", clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		}else{
		  alert('Ocorreu algum erro que impossibilitou a alteração, por favor, tente novamente!');
		}			
	}
	function importa(){
   
        location.href='importcsv.php?id=+<?php echo $id; ?>';
	  
	}
	
	function avisa(){
   
        location.href='preAvisaFornecedores.php?id=<?php echo $id; ?>';
	  
	}
	
	function resposta2(resp2){
	    var json2 = resp2.responseText;
		carregado();

		if (json2 == 'ok'){ 
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/itensCotacoes.php?id=+<?php echo $id; ?>" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		  document.palavras.reset();
		}else{
		  alert('Devido alguma falha, não foi efetivado o cadastro da popup!');
		}  
			  
	}
	
	function troca(ho){
	   location.href='mostracotacao.php?id='+ ho;
	}
</script>
<?php

}//fachando o teste se está logado!!
?>
<style type="text/css">		
	    @import "../dijit/themes/tundra/tundra.css";		
		@import "../dojox/grid/resources/Grid.css";		
		@import "../dojox/grid/resources/tundraGrid.css";		
	</style>
</head>

<body class="tundra">
 <div id="loader"><img src="img/loading.gif" />Carregando</div>
 <div id="modal"></div>
 <h1>Gestão de cotações automáticas - itens da cotação</h1>
 <?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
 if ($_SESSION['iduser'] > 0){
   //agora vamos ver qual o nível deste usuário
   require_once("../classes/Connection.php");
   $buscaN = new Connection();
   
   $nivel = $buscaN->buscaNivel($_SESSION['iduser']);//executando a função da classe Connection, que retorna o nível deste usuário
 
   if ($nivel == 0){	
    ?>
				   <div id="form" class="dialog">
					  <form name="palavras" method="post" dojoType="dijit.form.Form">
						 <label>Data</label>
						 <select name="cotacoes" onchange="troca(this.value);">
						     <option></option>
							 <?php
								 $resp = $query->getConsulta2("SELECT * FROM `cotacoes` ORDER BY `data` DESC");
								 while ($row = $resp->fetch_assoc()){
								     echo "<option value='".$row['idCotacao']."'>".$funcoes->trataData($row['data'])."</option>";
								 }
							 ?>
						 </select>
					  </form>
					  <div id="btns2">
							<button dojoType="dijit.form.Button" onClick="importa()" id="new">Importar arquivo csv</button>
							<button dojoType="dijit.form.Button" onClick="avisa()" id="avisa">Avisar Fornecedores</button>
					  </div>
				   </div> <!-- fecha o formulário -->			   
				   <div id="gridNode" jsId="grid1"></div>
  <?php
    } //fecha o if do nível de usuário
	 else{
	    echo "Usuário sem permissão para acessar esta função!";
	 }
 }else{
 	echo "Usuário não está logado!";
 }	 
   ?>		
</body>
</html>
