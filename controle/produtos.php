<?PHP
	// para controle de seguran�a, utilizaremos vari�veis de sess�o, isto aqui est� inicializando a sess�o
	session_start("login");
	require_once("../classes/Connection.php");
	
	$query = new Connection();
	
	$resp = $query->getConsulta2("SELECT * FROM `categoria` ORDER BY `categoria`");
	
	$i = 1;
    $linha = mysqli_num_rows($resp);
   
	$options = "[";
	$values  = "[";
	while ($row = $resp->fetch_assoc() ){
		$options .= "'".$row["categoria"]."'";
		$values .= "'".$row["idCategoria"]."'";
		if ($i == $linha){
			  $options.='';
			}
			else{
			  $options.=',';
			  $values.=',';
			}
			$i++;  
	}
	$options .="]";
	$values  .="]";
	
	//----------------marcas
	$respmarca = $query->getConsulta2("SELECT * FROM `marca` ORDER BY `Marca`");
	
	$i1 = 1;
    $linha1 = mysqli_num_rows($respmarca);
   
	$options1 = "[";
	$values1  = "[";
	while ($row1 = $respmarca->fetch_assoc() ){
		$options1 .= "'".$row1["Marca"]."'";
		$values1 .= "'".$row1["idMarca"]."'";
		if ($i1 == $linha1){
			  $options1.='';
			}
			else{
			  $options1.=',';
			  $values1.=',';
			}
			$i1++;  
	}
	$options1 .="]";
	$values1  .="]";
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Gest&atilde;o de produtos</title>
<script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
<script type="text/javascript" src="script/prototype.js"></script>
<?php
     //checando se tem alguem logado, �ltima tentativa de seguran�a. se n�o tiver logado, daqui n�o passa
if ($_SESSION['iduser'] > 0){
?>
<script type="text/javascript" src="script/defs.js"></script>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
  dojo.require("dijit.form.Button");
  dojo.require("dijit.form.TextBox");
  dojo.require("dojox.grid.DataGrid");
  dojo.require("dojo.data.ItemFileWriteStore");
  dojo.require("dijit.form.NumberTextBox");  
  dojo.require("dijit.form.FilteringSelect");
  dojo.require("dijit.form.CheckBox");
  
  dojo.require("dijit.Menu");
      
  idUserLogado = "<?php echo $_SESSION['iduser'] ?>";
  
  var pMenu = null;
  var rowValue = null;
  var count = 0;
  
  
  var idCategoria = null;
  var idMarca = null;
  
  var dialogo = null;  
  
  var grid = null; 
  
  dojo.addOnLoad(function(){	
	 	 //criando os argumentos do grid
         argumentos = define();
		
		 var jsonStore = new dojo.data.ItemFileWriteStore({ url: "json/produtos.php" , clearOnClose: true});  
		  
	     var layout= [	
		 		{ field: "part_num", width: "60px", name: "id", editable:false },
				{ field: "nmProduto", width: "200px", name: "Nm Produto", editable: true },
				{ field: "idCategoria", width: "150px", name: "Categoria", editable: true, type: dojox.grid.cells.Select, options: <?php echo $options; ?>, values: <?php echo $values; ?>  },
				{ field: "idMarca", width: "150px", name: "Marca", editable: true, type: dojox.grid.cells.Select, options: <?php echo $options1; ?>, values: <?php echo $values1; ?>  },
				{ field: "descricao1", width: "500px", name: "Descri��o principal", editable: true },
				{ field: "descricao2", width: "500px", name: "Descri��o secund�ria", editable: true },
				{ field: "valor", width: "100px", name: "Valor R$", editable: true },
				{ field: "mostraValor", width: "100px", name: "Mostrar R$?", editable: true, type: dojox.grid.cells.Select, options: ['Sim', 'N�o'], values: ['Sim', 'N�o'] },
				{ field: "status", width: "100px", name: "Status", editable: true, type: dojox.grid.cells.Select, options: ['Ativo', 'Inativo'], values: ['Ativo', 'Inativo'] }
				];
		 
		grid = new dojox.grid.DataGrid({
						Id: "grid1",
						singleClickEdit: false,
						store: jsonStore,
						structure: layout,
						loadingMessage:'Carregando Clientes',
						onApplyCellEdit: editEduRow,
						rowsPerPage: 20	},
						 'gridNode');
    	grid.store.close();
		 //grid.onApplyEdit(editando);
		grid.startup();
		dojo.connect(grid, "onKeyDown", clique);

		dojo.connect(grid, "onCellClick", dblClick);
		
		grid.onRowContextMenu = function(e){
			   
				var rowValue = grid.getItem(e.rowIndex).part_num;
				idReceita = rowValue;	
				
				nmReceita = grid.getItem(e.rowIndex).nmProduto;
		}
		
		//submenu
		
		pMenu = new dijit.Menu({
		    targetNodeIds: ["gridNode"]
		});
		
		pMenu.addChild(new dijit.MenuItem({
            label: "Enviar Fotos",
			onClick: function(){
			  abreFotos();
			}
        }));
		
        pMenu.startup();
		
		
		//drop down categorias
		var jsonStore2 = new dojo.data.ItemFileReadStore({ url: "json/mostraCategorias.php"}); 

		var filteringSelect = new dijit.form.FilteringSelect({
						id : "selectCategoria",
						store: jsonStore2,
						searchAttr: "categoria"					
					}, "selectCategoria");
				
		dojo.connect(filteringSelect, "onChange", selectCategoria);
		
		//drop down marcas
		var jsonStore3 = new dojo.data.ItemFileReadStore({ url: "json/mostraMarcas.php"}); 

		var filteringSelect2 = new dijit.form.FilteringSelect({
						id : "selectMarca",
						store: jsonStore3,
						searchAttr: "Marca"					
					}, "selectMarca");
				
		dojo.connect(filteringSelect2, "onChange", function(){
		   idMarca = dijit.byId('selectMarca').attr('value');
		});
		
	});//close the addonload
	
	function selectCategoria(){
	   idCategoria = dijit.byId('selectCategoria').attr('value');
	}
	
	function dblClick(e){
	  var row = grid.selection.getSelected()[0];
	  var campo = null;
      
	  //editando preparo
	  if (e.cellIndex == 4){campo = 'descricao2'}
	  if (campo !== null){
	    var value = grid.store.getValue(row,campo);
		editaTextos(campo, value);
	  } 	
	  
	  //editando ingredientes
	  if (e.cellIndex == 3){
	    campo = 'descricao1'
	    var value = grid.store.getValue(row,campo);
		//editaTextos(campo, value);
		idReceita = pegandoId(grid.selection.getSelected());
		
	  } 
	}
	
	function abreFotos(){
	  //var dlg =  dijit.byId("dialogTwo");	
	   criaDialogo('Upload de fotos: ');
	   var secondDlg =  dijit.byId(dialogoId);
	   
	   secondDlg.href = 'carrega.php?pg=foto.php&id='+idReceita;
	   secondDlg.show();
	      
	   }
	   
	function editorOne(e){
	  if (campoEmEdicao !== null){
	     campoEmEdicao = null;
	   }
	   
	   var secondDlg =  dijit.byId("dialogOne");	
	      	  
	   secondDlg.show();
	   var txtAnterior = preparo;
	   
	   if (preparo == '' || preparo  == null){dijit.byId("editor1").setValue('');}
	   else{dijit.byId("editor1").setValue(txtAnterior);}
	   //
	   
	}
	
	
	function criaDialogo(t){
	   count++;
	   dialogoId = 'caixaDialogo'+count;
	   
		   dialogo = new dijit.Dialog({
					title: t + nmReceita,
					style: "width: 900px; height: 480px; overflow: hidden",
					id:dialogoId
				});			
	}	
		
	function clique(tecla){   			
	  if(tecla.keyCode == 46){    //se a tecla foi o del, vamos deletar o item selecionado
       if (confirm('Deseja realmente excluir este Item e suas fotos?')){
		 var identificador = pegandoId(grid.selection.getSelected());
		 
			 var url = 'excluindoProdutos.php?id='+identificador+'&iduser='+idUserLogado;
			 retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
		   
		}//fecha confirm 
		 
	  }
	}
	
	function pegandoId(linhaSelecionada){
	  var arrayform = new Array();
	  if(linhaSelecionada.length){
            dojo.forEach(linhaSelecionada, function(selectedItem) {
                if(selectedItem !== null) {
					var ii = 0;					
                    dojo.forEach(grid.store.getAttributes(selectedItem), function(attribute) {
                        var value = grid.store.getValues(selectedItem, attribute);
                        
						arrayform[ii] = value;
						ii++; 
                    }); // end forEach
                } // end if
            }); // end forEach
        } // end if
		
		return arrayform[0];
	}
	
	function editEduRow(txt, index, campo){
		
		var identificador = pegandoId(grid.selection.getSelected());
		
		//utilzando m�dulo ajax do prototype
		var url = 'alterandoProdutos.php?id='+identificador+'&campo='+campo+'&txt='+txt;//variavel que vai mandar os dados via get 
		
		retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
	 
	}
		
	function carregando(){
	  /*
		$('loader').style.display='block';	
		$('modal').style.display='block';
	  */	
	}
	function carregado(){
		$('loader').style.display='none';	
		$('modal').style.display='none';
	}
	function falha(){
	  alert('falha no carregamento');
	  carregado();
	}
	
	function resposta(resp){
	    
		var json = resp.responseText;
		if (json == 'ok'){
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/produtos.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		}else{
		  alert('Ocorreu um erro que impossibilitou a altera��o, por favor, tente novamente!');
		}	
		carregado();
		preparo = null;
		campoEmEdicao = null;
		arrayIngredientes.length = 0;		
	}
	function cadastra(){	 
	  nome     = $('nome').value;
	  desc1    = $('descricao1').value;
	  desc2    = $('descricao2').value;
	  valor    = $('valor').value;
	  mostra =$('mostra').checked;
	    
	    	  	
	  checagem  = true;
	  
	  if (nome == ''){
		alert('Informe corretamente o nome');
		$('nome').focus();
		checagem = false;
	  }
	  else{	   
		  if (idCategoria <=0){
				alert('Selecione uma categoria!');
				$('selectCategoria').focus();
				checagem = false;
			}else{
			  if (idMarca <= 0){
			     alert('Selecione uma marca!');
				 $('selectMarca').focus();
				 checagem = false;
			  }
			}	
					  
	  }

	  if(checagem){
		var url2 = 'cadastrandoProduto.php?nome='+nome+'&descricao1='+desc1+'&idUser='+idUserLogado+'&descricao2='+desc2+'&idCategoria='+idCategoria+'&valor='+valor+'&mostra='+mostra+'&idMarca='+idMarca;
		
		retorno2 = new Ajax.Request(url2, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta2});
	  }
	}
	
	function resposta2(resp2){
	    var json2 = resp2.responseText;
		if (json2 == 'ok'){ 
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/produtos.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		  limpa();
		}else{
		  alert('Devido falha, n�o foi efetivado o cadastro!');
		}  
		carregado();
			  
	}
	
	function limpa(){
	   document.formulario.reset();
	}
	function filtrar(n){
	   //var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/produtos.php" , clearOnClose: true}); 
	   //grid.setStore(jsonStore2);
	   if (n == 1){
	    var cate = dijit.byId('selectCategoria').item.categoria;		
		grid.filter({idCategoria: cate+"*"}); 
	   }
	    if (n == 2){
	    var mark = dijit.byId('selectMarca').item.Marca;		
		grid.filter({idMarca: mark+"*"}); 
	   }
	   //grid.filter({nmProduto: "A*"});
	}
	function todos(){
	  grid.filter({nmProduto: "*"});
	}
</script>
<?php

}//fachando o teste se est� logado!!
?>
<style type="text/css">		
	    @import "../dijit/themes/tundra/tundra.css";		
		@import "../dojox/grid/resources/Grid.css";		
		@import "../dojox/grid/resources/tundraGrid.css";		
</style>
<link href="css2.css" rel="stylesheet" type="text/css" />

</head>

<body class="tundra">
 <div id="loader"><img src="img/loading.gif" /><br />Carregando</div>
 <div id="modal"></div>
 <h1>Gest�o de Produtos/estoque</h1>
 <?php
 if ($_SESSION['iduser'] > 0){

   require_once("../classes/Connection.php");
   $buscaN = new Connection();
   
   $nivel = $buscaN->buscaNivel($_SESSION['iduser']);//executando a fun��o da classe Connection, que retorna o n�vel deste usu�rio
 
   if ($nivel == 0){	
    ?>	           <!--tipo eventos-->
				   <div id="form" class="dialog">
					  <form name="formulario" method="post" dojoType="dijit.form.Form">
					    <table width="100%" border="0" cellspacing="3" cellpadding="0">
						  <tr>
							<td>Categoria</td>
							<td><div id="selectCategoria"></div><button dojoType="dijit.form.Button" onclick="filtrar(1);" id="filtra1">Filtro</button></td>
							<td>Marca</td>
							<td><div id="selectMarca"></div><button dojoType="dijit.form.Button" onclick="filtrar(2);" id="filtra2">Filtro</button></td>
						  </tr>
						  <tr>
							<td>Nome</td>
							<td><input dojoType="dijit.form.TextBox" name="nome" id="nome" /></td>
							<td>Descri��o 1</td>
							<td><input dojoType="dijit.form.TextBox" name="descricao1" id="descricao1" /></td>
						  </tr>
						  <tr>
							<td>Descri��o 2</td>
							<td><input dojoType="dijit.form.TextBox" name="descricao2" id="descricao2" /></td>
							<td>Valor</td>
							<td><input dojoType="dijit.form.NumberTextBox" name="valor" id="valor"  style="width:80px"/><label for='mostra'>Mostrar Valor?</label><input dojoType='dijit.form.CheckBox' name="mostra" id="mostra" /></td>
						  </tr>
						</table>
					  </form>
					  <br />
					  <div id="btns3">
							<button dojoType="dijit.form.Button" onClick="cadastra()" id="new">Novo</button>
							<button dojoType="dijit.form.Button" onclick="limpa();" id="limpa">Limpar</button>
							<button dojoType="dijit.form.Button" onclick="todos();" id="tuto">Mostrar Todos</button>
					  </div>
				   </div> <!-- fecha o formul�rio -->			   
				   <div id="gridNode" ></div>
  <?php
    } //fecha o if do n�vel de usu�rio
	 else{
	    echo "Usu�rio sem permiss�o para acessar esta fun��o!";
	 }
 }else{
 	echo "Usu�rio n�o est� logado!";
 }	 
   ?>	
</body>
</html>
