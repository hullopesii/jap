<?PHP
	// para controle de segurança, utilizaremos variáveis de sessão, isto aqui está inicializando a sessão
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gestão de usuários</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
<script type="text/javascript" src="script/prototype.js"></script>
<?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
if ($_SESSION['iduser'] > 0){
?>
<script type="text/javascript" src="script/defs.js"></script>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
  dojo.require("dijit.form.Button");
  dojo.require("dijit.form.TextBox");
  dojo.require("dojox.form.DropDownSelect");
  dojo.require("dojox.grid.DataGrid");
  dojo.require("dojo.data.ItemFileWriteStore");
   
  idUserLogado = "<?php echo $_SESSION['iduser'] ?>";
  var argumentos = new Array();
  
  var grid = null; 
  dojo.addOnLoad(function(){	
	 	 //criando os argumentos do grid
         argumentos = define();
		
		 var jsonStore = new dojo.data.ItemFileWriteStore({ url: "json/usuarios.php" , clearOnClose: true});  
		  
	     var layout= [	
		 		{ field: "part_num", width: "10%", name: "id", editable:false },
				{ field: "nome", width: "30%", name: "Nome", editable: true }, 
				{ field: "user", width: "30%", name: "User Name", editable: true },
				{ field: "nivel", width: "10%", name: "Nível", editable: true, type: dojox.grid.cells.Select, options: [ 'Administrador', 'Usuario' ], values: ['0', '1'] }
				];
		 
		grid = new dojox.grid.DataGrid({
						Id: "grid1",
						singleClickEdit: false,
						store: jsonStore,
						structure: layout,
						loadingMessage:'Carregando',
						onApplyCellEdit: editEduRow,
						rowsPerPage: 20	},
						 'gridNode');
		 grid.store.close();
		 //grid.onApplyEdit(editando);
		grid.startup();
		dojo.connect(grid, "onKeyPress", clique);//tipo o addeventListener, vai chamar a função clique toda vez que uma tecla for clicada, quando uma linha da grid estiver selecionada
	});
	
	function clique(tecla){   			
	  if(tecla.keyCode == 46){    //se a tecla foi o del, vamos deletar o item selecionado
       if (confirm('Deseja realmente excluir este usuário?')){
		 var identificador = pegandoId(grid.selection.getSelected());
		 if (identificador == idUserLogado){
		   alert('Você não pode excluir seu próprio usuário!');
		 }else{
			 var url = argumentos[1]+'?id='+identificador+'&iduser='+idUserLogado;
                        window.open(url,"_self");
			 //retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
		  }	 
		}//fecha confirm 
		 
	  }
	}
	
	function pegandoId(linhaSelecionada){
	  var arrayform = new Array();
	  if(linhaSelecionada.length){
            dojo.forEach(linhaSelecionada, function(selectedItem) {
                if(selectedItem !== null) {
					var ii = 0;					
                    dojo.forEach(grid.store.getAttributes(selectedItem), function(attribute) {
                        var value = grid.store.getValues(selectedItem, attribute);
                        
						arrayform[ii] = value;
						ii++; 
                    }); // end forEach
                } // end if
            }); // end forEach
        } // end if
		
		return arrayform[0];
	}
	
	function editEduRow(txt, index, campo){
		//editando no banco de dados, utilizando o framework prototype
		
		//pegando o id, usando a função pegandoId, onde passamos para ela a linha selecionada, e ela retorna para o identificador 
		var identificador = pegandoId(grid.selection.getSelected());
		
		//utilzando módulo ajax do prototype
		var url = 'alterandoUsuarios.php?id='+identificador+'&campo='+campo+'&txt='+txt;//variavel que vai mandar os dados via get 
		//blz, criamos a variavel url, que contem o nome da página php que vai fazer alteração no banco de dados
		//window.open(url,"_self");
		//aqui mandamos via ajax, acionando a página definida na "url"
		retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
	 
	}
	
	function carregando(){
		$('loader').style.visibility='visible';	
		$('modal').style.visibility='visible';
	}
	function carregado(){
		$('loader').style.visibility='hidden';	
		$('modal').style.visibility='hidden';
	}
	function falha(){
	  alert('falha no carregamento');
	  carregado();
	}
	function resposta(resp){
	    carregado();
		var json = resp.responseText;//pegando o texto retornado pela página de alteração
		if (json == 'ok'){
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/usuarios.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		}else{
		  alert('Ocorreu algum erro que impossibilitou a alteração, por favor, tente novamente!');
		}			
	}
	function cadastraUser(){

	  nome     = $('nomeUser').value;
	  username = $('userName').value;
	  nivel    = dijit.byId('nivelUser').attr('value');
	  	
	  checagem  = true;
	  
	  if (nome == ''){

		alert('Informe corretamente o nome do usuário');
		$('nomeUser').focus();
		checagem = false;
	  }
	  else{
	    if (username == ''){
		  	alert('Informe corretamente o User Name');
			$('userName').focus();
			checagem = false;
		}	   
	  }

	  if(checagem){
		var url2 = argumentos[0]+'?nm='+nome+'&nameuser='+username+'&nivel='+nivel+'&idUser='+idUserLogado;
                //window.open(url2,"_self");
                retorno2 = new Ajax.Request(url2, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta2});
	  }
	}
	
	function resposta2(resp2){
	    var json2 = resp2.responseText;
		carregado();

		if (json2 == 'ok'){ 
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/usuarios.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		  document.usuarios.reset();
		}else{
		  alert('Devido alguma falha, não foi efetivado o cadastro do usuário!');
		}  
			  
	}
</script>
<?php

}//fachando o teste se está logado!!
?>
<style type="text/css">		
	    @import "../dijit/themes/tundra/tundra.css";		
		@import "../dojox/grid/resources/Grid.css";		
		@import "../dojox/grid/resources/tundraGrid.css";		
	</style>
</head>

<body class="tundra">
 <div id="loader"><img src="img/loading.gif" />Carregando</div>
 <div id="modal"></div>
 <h1>Gestão de Usuários</h1>
 <?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
 if ($_SESSION['iduser'] > 0){
   //agora vamos ver qual o nível deste usuário
   require_once("../classes/Connection.php");
   $buscaN = new Connection();
   
   $nivel = $buscaN->buscaNivel($_SESSION['iduser']);//executando a função da classe Connection, que retorna o nível deste usuário
 
   if ($nivel == 0){	
    ?>
				   <div id="form" class="dialog">
					  <form name="usuarios" method="post" dojoType="dijit.form.Form">
						 <label>Nome</label><input dojoType="dijit.form.TextBox" name="nome" id="nomeUser" />
						 <label>User Name</label><input dojoType="dijit.form.TextBox" name="user" id="userName"  />
						 <label>Nível</label><select dojoType="dojox.form.DropDownSelect" name="nivel" id="nivelUser">
												 <option value="0">Administrador</option>
												 <option value="1">Usuário</option>
											 </select>
						 <br />
					  </form>
					  <div id="btns2">
							<button dojoType="dijit.form.Button" onClick="cadastraUser()" id="newUser">Novo</button>
							<button dojoType="dijit.form.Button" onclick="document.usuarios.reset();" id="limpa">Limpar</button>
					  </div>
				   </div> <!-- fecha o formulário -->			   
				   <div id="gridNode" jsId="grid1"></div>
  <?php
    } //fecha o if do nível de usuário
	 else{
	    echo "Usuário sem permissão para acessar esta função!";
	 }
 }else{
 	echo "Usuário não está logado!";
 }	 
   ?>		
</body>
</html>
