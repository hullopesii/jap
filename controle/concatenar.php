 <?PHP
 /*
  session_start("login");
  
  if (empty($_SESSION['iduser'])){
	  $_SESSION['iduser'] = 0;
	   header("Location:index.php");
	}
	*/
	$url = $_SERVER["REQUEST_URI"];
	
	$exurl = explode("?",$url);
	
	$cods = $exurl[1];
	
	$arrCotacoes = array();
	
	//receber o i, que � a quantidade de cota��es
	$i = $_GET['i'];
	$ids = '';
	for ($c=1;$c<=$i;$c++){
	    $arrCotacoes[] = $_GET['cotacao'.$c];
		$ids .= $_GET['cotacao'.$c];
		if ($c<$i)
			$ids .= ', ';
	}
	
	
	require_once("../classes/Connection.php");
	$query = new Connection();
	
	$resp = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE `idCotacao` in ({$ids})");
	//echo "SELECT * FROM `cotacoes` WHERE `idCotacao` in ({$ids})";
	//$row = $resp->fetch_assoc();
	
	require_once("../classes/Funcoes.php");
	$funcoes = new Funcoes();
	
	require_once("../classes/Listagem.php");
	$listagem = new Listagem();
	/*
	$listagem->contaGanhos($id);*/
	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Concatenando Cota��es para Envio aos fornecedores</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script src="../scripts/jquery.1.5.2.min.js"></script>
    <script src="../scripts/jquery.tablesorter.min.js"></script>
    <script src="../scripts/jquery.tablesorter.pager.js"></script>
	<script type="text/javascript">
		function avisa(){
				cods = '<?php echo $cods; ?>';
				location.href='preAvisaFornecedores2.php?'+cods;
				
		}
	</script>
<style>
  .porta1 a{width:200px; height:100px; float:left; background:#CCCCCC; margin:5px; padding:2px; text-align:center;}
  table{
  width:98%;
  border:1px solid #ccc;
  font-size:12px;
}

table thead th{
  background-color:#ad2525;
  padding:6px;
  color:#fff;
  text-align:center;
  font-size:12px;
}

table thead th.header{
  cursor:pointer;
}

table tbody td{
  padding:6px;
  text-align:center;
  color:#333;
}

table tbody tr.odd td{
  background-color:#ffffcc;
}

table tbody tr.hover td{
  background-color:#a9d0f5;
}

table tbody tr.selected td{
  background-color:#a9f5a9!important;
}

.pagedisplay{
	width:40px;
	text-align:center;
	border:none;
	background-color:#f2f2f2;
	font-weight:900;
}

#pager span{
	font-weight:900;
	display:inline-block;
	margin:0 0 0 20px;
	color:#666;
	float:right;
}

#pager form{
	text-align:left;
	padding:10px;
	width:580px;
}
a img{
	border:none;
}

form{
  background-color:#f2f2f2;
  width:600px;
  margin:10px 0 10px 0;
  text-align:center;
}

form p{
  padding:12px;
}

form p label{
  color:#333;
  font-weight:900;
}

form input{
  padding:6px;
  border:1px solid #ccc;
  width:300px;
}
</style>
</head>

<body>
<h1>Concatenando Cotações -- Código(s): <?php echo $ids; ?></h1>
<?php
  while ($row = $resp->fetch_assoc()){
  		echo "<h3>Cotação código " .$row['idCotacao']. " - ".$row['loja']." - ".$funcoes->trataData($row['data'])."</h3>";
  }
?>
  <button onClick="avisa()" id="avisa">Avisar Fornecedores</button>
  
  <table width="100%" border="0" cellspacing="3" cellpadding="3">
    <thead>
	  <tr>
		<td width="5%" bgcolor="#CCCCCC">C&oacute;digo</td>
		<td width="40%" bgcolor="#CCCCCC">Descri&ccedil;&atilde;o</td>
		<td width="10%" bgcolor="#CCCCCC">Marca</td>
		<td width="5%" bgcolor="#CCCCCC">Qtd Cotada</td>
	  </tr>
	 </thead> 
  <tbody>
  <?php
      
      $res = $query->getConsulta2("SELECT * , SUM( qtdCotada ) AS  'total' FROM  `itenscotacoes` WHERE idCotacao IN ({$ids}) GROUP BY codigo");
	  $html = '';
	  while ($r = $res->fetch_assoc()){
		 $html.=' <tr>
					<td>'.$r['codigo'].'</td>
					<td  style="text-align:left">'.$r['descricao'].'</td>
					<td  style="text-align:left">'.$r['referencia'].'</td>
					<td>'.$r['total'].'</td>
				  </tr>';
	  }
	  echo $html;
  ?>
  </tbody>
</table>
<div id="pager" class="pager">
    	<form>
				<span>
					Exibir <select class="pagesize">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
					</select> registros
				</span>

				<img src="img/first.png" class="first"/>
    		<img src="img/prev.png" class="prev"/>
    		<input type="text" class="pagedisplay"/>
    		<img src="img/next.png" class="next"/>
    		<img src="img/last.png" class="last"/>
    	</form>
    </div>

<script>
    $(function(){
      
      $('table > tbody > tr:odd').addClass('odd');
      
      $('table > tbody > tr').hover(function(){
        $(this).toggleClass('hover');
      });
      
      $('#marcar-todos').click(function(){
        $('table > tbody > tr > td > :checkbox')
          .attr('checked', $(this).is(':checked'))
          .trigger('change');
      });
      
      $('table > tbody > tr > td > :checkbox').bind('click change', function(){
        var tr = $(this).parent().parent();
        if($(this).is(':checked')) $(tr).addClass('selected');
        else $(tr).removeClass('selected');
      });
      
      $('form').submit(function(e){ e.preventDefault(); });
      
      $('#pesquisar').keydown(function(){
        var encontrou = false;
        var termo = $(this).val().toLowerCase();
        $('table > tbody > tr').each(function(){
          $(this).find('td').each(function(){
            if($(this).text().toLowerCase().indexOf(termo) > -1) encontrou = true;
          });
          if(!encontrou) $(this).hide();
          else $(this).show();
          encontrou = false;
        });
      });
      
      $("table") 
        .tablesorter({
          dateFormat: 'uk',
          headers: {
            0: {
              sorter: false
            },
            5: {
              sorter: false
            }
          }
        }) 
        .tablesorterPager({container: $("#pager")})
        .bind('sortEnd', function(){
          $('table > tbody > tr').removeClass('odd');
          $('table > tbody > tr:odd').addClass('odd');
        });
      
    });
    </script>

</body>
</html>
