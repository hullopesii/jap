 <?PHP
	
	
	$id = $_REQUEST['id'];
	$idF = $_REQUEST['idF'];
	
	require_once("../classes/Connection.php");
	$query = new Connection();
	
	$resp = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE `idCotacao`={$id}");
	$row = $resp->fetch_assoc();
	
	require_once("../classes/Funcoes.php");
	$funcoes = new Funcoes();
	
	require_once("../classes/Listagem.php");
	$listagem = new Listagem();
	
	$post = $_POST;
	$Ids = '';
	foreach($post as $p){
			$Ids.=$p.',';
	}
	$Ids = substr($Ids, 0, -1);//retirando a Ãºltima vÃ­rgula
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Respostas dos fornecedores</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script src="../scripts/jquery.1.5.2.min.js"></script>
    <script src="../scripts/jquery.tablesorter.min.js"></script>
    <script src="../scripts/jquery.tablesorter.pager.js"></script>
	<script type="text/javascript">
	      function submita(){
			  document.getElementById("pedido").submit();
		   }
	</script>
<style>
  .porta1 a{width:200px; height:100px; float:left; background:#CCCCCC; margin:5px; padding:2px; text-align:center;}
  table{
  width:98%;
  border:1px solid #ccc;
  font-size:12px;
}

table thead th{
  background-color:#ad2525;
  padding:6px;
  color:#fff;
  text-align:center;
  font-size:12px;
}

table thead th.header{
  cursor:pointer;
}

table tbody td{
  padding:6px;
  text-align:center;
  color:#333;
}

table tbody tr.odd td{
  background-color:#ffffcc;
}

table tbody tr.hover td{
  background-color:#a9d0f5;
}

table tbody tr.selected td{
  background-color:#a9f5a9!important;
}

.pagedisplay{
	width:40px;
	text-align:center;
	border:none;
	background-color:#f2f2f2;
	font-weight:900;
}

#pager span{
	font-weight:900;
	display:inline-block;
	margin:0 0 0 20px;
	color:#666;
	float:right;
}

#pager form{
	text-align:left;
	padding:10px;
	width:580px;
}
a img{
	border:none;
}

form{
  background-color:#f2f2f2;
  width:600px;
  margin:10px 0 10px 0;
  text-align:center;
}

form p{
  padding:12px;
}

form p label{
  color:#333;
  font-weight:900;
}

form input{
  padding:6px;
  border:1px solid #ccc;
  width:300px;
}
.menor{ color:#990000;}
.total{ float:right; font-size:18px; margin-right:40px;}
</style>
</head>

<body>
<h1>Fechando o pedido da cotação - Cotação código <?php echo $row['idCotacao']." data ". $funcoes->trataData($row['data']); ?></h1>
  <?php     
      //buscando os fornecedores
	  $respFornecedores = $query->getConsulta2("SELECT * FROM `fornecedores` WHERE `idFornecedor`={$idF}");
	  $rowFornecedores = $respFornecedores->fetch_assoc();
	  echo "<a href='mostrarespostas.php?id={$id}' title='Clique para ver a listagem de todos os fornecedores'>Voltar à home</a>";
	  echo "<h3>Pedido enviado para ".$rowFornecedores['fornecedor']."</h3>";
	  echo $emailFornecedor = $rowFornecedores['email'];
	  
	  function pegaNomeF($valor, $idP){
				  $query = new Connection();
				  if ($valor > 0){
					  $r = $query->getConsulta2("SELECT * FROM `respitemcotacao` inner join fornecedores on fornecedores.idFornecedor = respitemcotacao.idFornecedor WHERE (respitemcotacao.preco= {$valor} and respitemcotacao.idItemCotacao={$idP})");
					   $html = '';
					  while ($row = $r->fetch_assoc()){
						$html.= $row['fornecedor'].',';
					  }
					  return substr($html, 0, -1);
				   }	   
			  }
			  
			  function pegaMinimo($idp){
				  $query = new Connection();
				  $r = $query->getConsulta2("SELECT *, MIN(preco) as 'min',  MAX(preco) as 'max' FROM `respitemcotacao` WHERE idItemCotacao = {$idp} ");
				  $ar = array();
				  if ($r->num_rows > 0){
					 $rw = $r->fetch_assoc();
					 $ar['min'] = $rw['min'];
					 $ar['max'] = $rw['max'];
				  }
				  return $ar;
			  }
	  
  ?>
  
  <table width="100%" border="0" cellspacing="3" cellpadding="3">
    <thead>
	  <tr>
		<td width="5%" bgcolor="#CCCCCC">C&oacute;digo</td>
		<td width="50%" bgcolor="#CCCCCC">Descri&ccedil;&atilde;o</td>
		<td width="10%" bgcolor="#CCCCCC">Marca</td>
		<td width="5%" bgcolor="#CCCCCC">Qtd Cotada</td>
		<td width="8%" bgcolor="#CCCCCC">Valor Un</td>
		<td width="8%" bgcolor="#CCCCCC">Total</td>
	  </tr>
	 </thead> 
		  <form name="pedido" id="pedido" action="mandaPedido2.php<?php echo $id; ?>&idF=<?php echo $idF; ?>" method="post">
		  <tbody>
		  <?php
				   			
			  $res = $query->getConsulta2("SELECT * FROM `respitemcotacao` WHERE `idRespCotacao` in ({$Ids})");
			  $html = '';
			  $total = 0;
			  while ($r = $res->fetch_assoc()){
				 $r['idItemCotacao'];
				 $mima = pegaMinimo($r['idItemCotacao']);
				 //pegando nome do fornecedor
				 $title = pegaNomeF($mima['min'], $r['idItemCotacao']);
				 $title2= pegaNomeF($mima['max'], $r['idItemCotacao']);
				 
				 $rress = $query->getConsulta2("SELECT * FROM `itenscotacoes` WHERE `idItem`=". $r['idItemCotacao']);
				 $rr = $rress->fetch_assoc();
				 $total = $total+($mima['min']*$rr['qtdCotada']);
				 $html.=' <tr>
							<td>'.$rr['codigo'].'</td>
							<td  style="text-align:left">'.$rr['descricao'].'</td>
							<td  style="text-align:left">'.$rr['referencia'].'</td>
							<td>'.$rr['qtdCotada'].'</td>
							<td title="'.$title.'" class="menor">'.number_format($mima['min'], 2, ",", ".").'</td>
							<td>'.number_format($mima['min']*$rr['qtdCotada'], 2, ",", ".").'</td>
						  </tr>';
			  }
			  echo $html;
			  
			  //0---------------------------mandando o e-mail
			  
			   require_once("../PHPMailer_v5.1/class.phpmailer.php");
			   $mail = new PHPMailer(true);
				   
			   try{
					  $msg = '';
					  $mail->AddAddress($emailFornecedor);
					  $mail->CharSet = 'UTF-8';
					  $mail->SetFrom("cotacao@jataiautopecas.com.br", "Jataí Auto Peças");
					  $mail->AddCC('cotacao@jataiautopecas.com.br', 'Cópia do pedido gerado -'.$rowFornecedores['fornecedor']);
					  $mail->Subject = 'Novo pedido de peças gerado. Jataí Auto Peças';
					  $msg = file_get_contents('../email/cabecalho.php');
					  $msg.='<h4>Novo pedido de pe&ccedil;as/produtos. Jata&iacute; Auto Pe&ccedil;as</h4>';
					 
					  $msg.= "<h2>Novo pedido da Jata&iacute; Auto Pe&ccedil;as</h2>";
					  $msg.= "<p>".$rowFornecedores['fornecedor'].", Segue abaixo a rela&ccedil;&atilde;o dos produtos pedidos:</p>";   
					  $msg.=' <table width="100%" border="0" cellspacing="3" cellpadding="3">
								<thead>
								  <tr>
									<td width="5%" bgcolor="#CCCCCC">C&oacute;digo</td>
									<td width="50%" bgcolor="#CCCCCC">Descri&ccedil;&atilde;o</td>
									<td width="10%" bgcolor="#CCCCCC">Marca</td>
									<td width="5%" bgcolor="#CCCCCC">Qtd Cotada</td>
									<td width="8%" bgcolor="#CCCCCC">Valor Un</td>
									<td width="8%" bgcolor="#CCCCCC">Total</td>
								  </tr>
								 </thead> 
								 <tbody>
								 ';        
					  $msg.= $html;	  
					  $msg.="</tbody></table>"; 
					  $msg.='<div class="total">Total do Pedido R$ '.number_format($total, 2, ",", ".").'</div>';
					  $msg.= '</div>
							  </body>';
					  
					  $mail->MsgHTML($msg);
					  
					  $mail->Send();
					  
					  
				} catch (phpmailerException $e) {
				  $respEnvio = "Ocorreu um erro que impossibilitou o envio. Recarregue a pÃ¡gina e tente novamente!";
				  //echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
				  //echo $e->getMessage(); //Boring error messages from anything else!
				}
		  
 	
			  
			  //---------------------------------------------
		  ?>
		   </tbody>
		 </form>  
	</table>
	<div class="total">Total do Pedido R$ <?php echo number_format($total, 2, ",", "."); ?></div>

<script>
    $(function(){
      
      $('table > tbody > tr:odd').addClass('odd');
      
      $('table > tbody > tr').hover(function(){
        $(this).toggleClass('hover');
      });
      
      $('#marcar-todos').click(function(){
        $('table > tbody > tr > td > :checkbox')
          .attr('checked', $(this).is(':checked'))
          .trigger('change');
      });
      
      $('table > tbody > tr > td > :checkbox').bind('click change', function(){
        var tr = $(this).parent().parent();
        if($(this).is(':checked')) $(tr).addClass('selected');
        else $(tr).removeClass('selected');
      });
      
      $('form').submit(function(e){ e.preventDefault(); });
      
      $('#pesquisar').keydown(function(){
        var encontrou = false;
        var termo = $(this).val().toLowerCase();
        $('table > tbody > tr').each(function(){
          $(this).find('td').each(function(){
            if($(this).text().toLowerCase().indexOf(termo) > -1) encontrou = true;
          });
          if(!encontrou) $(this).hide();
          else $(this).show();
          encontrou = false;
        });
      });
      
      
      
    });
    </script>

</body>
</html>
