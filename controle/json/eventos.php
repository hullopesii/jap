<?php
  header("Content-Type: text/plain; charset=ISO-8859-1", true); // sa�da de dados com caracteres especiais
  //bloco anti cache
  header("Expires: Fri, 25 Dec 1980 00:00:00 GMT");
  header("Last-Modified: " . gmdate('D, d M Y H:i:s') . 'GMT');
  header('Cache-Control: no-cache, must-revalidate');
  header('Pragma: no-cache');
  //------------------------------------------------------------
   
   require_once("../../classes/Connection.php");
   
   $query = new Connection();
   
   require_once("../../classes/Funcoes.php");
   
   $func = new Funcoes();
   
   $sql = "SELECT * FROM `eventos` ORDER BY `data` Desc";

   $dado='';
   $resposta = $query->getConsulta2($sql);//executando a busca, usando fun��o p�blica da classe connection
   //controle da virgula que ser� usada na cria��o do json
   $i = 1;
   $linha = mysqli_num_rows($resposta);
   //----------------------------------------
   while ($row = $resposta->fetch_assoc() ){ // varrendo todas as linhas do array gerado pela busca
	 
	  $data = $func->trataData($row["data"]);
	 
	 //criando a vari�vel string que conter� o formato json
	 $dado .= "{ part_num: '".$row["idEvento"]."', evento:\"".$row["evento"]."\", descricao: \"". $row["descricao"]."\", data: \"". $data."\"}";
		//incluindo a v�rgula
		if ($i == $linha){
			  $dado.='';
			}
			else{
			  $dado.=',';
			}
			$i++;  
   }//fecha o while
   
   
   
   $init =  "{
               identifier: 'part_num',
   			   label: 'part_num',
			   items: [
			   ".$dado."]}";
   
   echo $init;
   
?>
