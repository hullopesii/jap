<?PHP
	session_start();
  if (empty($_SESSION['iduser'])){
	  $_SESSION['iduser'] = 0;
	   header("Location:index.php");
	}
?>
<!DOCTYPE html>
<html lang="pt">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="pt-br">
<title>Cotaçoes</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false, loale:'pt'" data-dojo-config="locale:'pt'"></script>
<script type="text/javascript" src="script/prototype.js"></script>
<?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
if ($_SESSION['iduser'] > 0){
?>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
  dojo.require("dijit.form.Button");
  dojo.require("dijit.form.TextBox");
  dojo.require("dojox.form.DropDownSelect");
  dojo.require("dojox.grid.DataGrid");
  dojo.require("dijit.form.DateTextBox");
  dojo.require("dojo.data.ItemFileWriteStore");
  dojo.require("dijit.Menu");
   
  idUserLogado = "<?php echo $_SESSION['iduser'] ?>";

  var idCotacao = 0;
  count = 0;
  var grid = null; 
  dojo.addOnLoad(function(){	
		
		 var jsonStore = new dojo.data.ItemFileWriteStore({ url: "json/cotacoes.php" , clearOnClose: true});  
		  
	     var layout= [	
		 		{ field: "part_num", width: "10%", name: "id", editable:false },
				{ field: "loja", width: "25%", name: "Loja", editable: true }, 
				{ field: "data", width: "25%", name: "Data", editable: true }, 
				{ field: "status", width: "25%", name: "Status", editable: true, type: dojox.grid.cells.Select, options: [ 'Ativa', 'Completa' ]  },
				{ field: "qtd", width: "15%", name: "Itens", editable: false}
				];
		 
		grid = new dojox.grid.DataGrid({
						Id: "grid1",
						singleClickEdit: false,
						store: jsonStore,
						structure: layout,
						loadingMessage:'Carregando',
						onApplyCellEdit: editEduRow,
						rowsPerPage: 20	},
						 'gridNode');
		 grid.store.close();
		 //grid.onApplyEdit(editando);
		grid.startup();
		dojo.connect(grid, "onKeyPress", clique);
		grid.onRowContextMenu = function(e){
			   
				var rowValue = grid.getItem(e.rowIndex).part_num;
				idCotacao = rowValue;	
		}
		
		//submenu
		
		pMenu = new dijit.Menu({
		    targetNodeIds: ["gridNode"]
		});
		
		pMenu.addChild(new dijit.MenuItem({
            label: "Itens da cotação",
			onClick: function(){
			  location.href='mostracotacao.php?id='+idCotacao;
			}
        }));
		
		pMenu.addChild(new dijit.MenuItem({
            label: "Respostas dos fornecedores",
			onClick: function(){
			  location.href='mostrarespostas.php?id='+idCotacao;
			}
        }));
		
        pMenu.startup();
	});
	
	function clique(tecla){   			
	  if(tecla.keyCode == 46){    //se a tecla foi o del, vamos deletar o item selecionado
       if (confirm('Deseja realmente excluir esta cotação?')){
		 var identificador = pegandoId(grid.selection.getSelected());
		 
			 var url = 'excluindoCatacoes.php?id='+identificador+'&iduser='+idUserLogado;
			 
			 retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
		     
		}//fecha confirm 
		 
	  }
	}
	function criaDialogo(t){
	   count++;
	   dialogoId = 'caixaDialogo'+count;
	   
		   dialogo = new dijit.Dialog({
					title: t,
					style: "width: 900px; height: 480px; overflow: hidden",
					id:dialogoId
				});			
	}	
	function regras(){
	  //var dlg =  dijit.byId("dialogTwo");	
	   criaDialogo('Editor de regras para a cotação/pedido');
	   var secondDlg =  dijit.byId(dialogoId);
	   
	   secondDlg.href = 'carrega.php?pg=editaregra.php&id=0';
	   secondDlg.show();
	      
	   }
	
	function pegandoId(linhaSelecionada){
	  var arrayform = new Array();
	  if(linhaSelecionada.length){
            dojo.forEach(linhaSelecionada, function(selectedItem) {
                if(selectedItem !== null) {
					var ii = 0;					
                    dojo.forEach(grid.store.getAttributes(selectedItem), function(attribute) {
                        var value = grid.store.getValues(selectedItem, attribute);
                        
						arrayform[ii] = value;
						ii++; 
                    }); // end forEach
                } // end if
            }); // end forEach
        } // end if
		
		return arrayform[0];
	}
	function pegandoIdMult(linhaSelecionada){
	  var arrayform = new Array();
	  
		for(i=0; i<linhaSelecionada.length;i++){
			id = grid.store.getValues(linhaSelecionada[i], 'part_num');
			arrayform[i]=id;
		}
		
		return arrayform;
	}
	
	function editEduRow(txt, index, campo){
		
		var identificador = pegandoId(grid.selection.getSelected());

		var url = 'alterandoCotacoes.php?id='+identificador+'&campo='+campo+'&txt='+txt;
		
		retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
	 
	}
	
	function carregando(){
		$('loader').style.visibility='visible';	
		$('modal').style.visibility='visible';
		dojo.byId("new").disabled=true;
	}
	function carregado(){
		$('loader').style.visibility='hidden';	
		$('modal').style.visibility='hidden';
		dojo.byId("new").disabled=false;
	}
	function falha(){
	  alert('falha no carregamento');
	  carregado();
	}
	function resposta(resp){
	    carregado();
		var json = resp.responseText;//pegando o texto retornado pela página de alteração
		if (json == 'ok'){
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/cotacoes.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		}else{
		  alert('Ocorreu algum erro que impossibilitou a alteração, por favor, tente novamente!');
		}			
	}
	function cadastraPop(){

	  data = $('data').value;
	  loja = $('loja').value;
	  	
	  checagem  = true;
	  
	  if (data == ''){
		alert('Informe corretamente a data da cotação');
		$('data').focus();
		checagem = false;
	  }else{
	  	if (loja == ''){
			alert('Informe corretamente a loja');
			$('loja').focus();
			checagem = false;
		}
	  }  

	  if(checagem){
		var url2 = 'cadastrandoCotacao.php?data='+data+'&idUser='+idUserLogado+'&loja='+loja;
		//location.href = url2; 
		retorno2 = new Ajax.Request(url2, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta2});
	  }
	}
	
	function resposta2(resp2){
	    var json2 = resp2.responseText;
		carregado();

		if (json2 == 'ok'){ 
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/cotacoes.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		  document.palavras.reset();
		}else{
		  alert('Devido alguma falha, não foi efetivado o cadastro da cotação!');
		}  
			  
	}
	function concatenar(){
		
		ids = pegandoIdMult(grid.selection.getSelected());
		strIds = '';
		ii = 0;
		for(i=0;i<ids.length;i++){
			    if (i == 0){
						strIds = strIds+'?cotacao1='+ids[i];
				}else{		
					strIds = strIds+'&cotacao'+(i+1)+'='+ids[i];
				}	
				ii =i;
		}
		if (ids.length >0){
			strIds = strIds+'&i='+(ii+1);
			location.href='concatenar.php'+strIds;
		}else{
			alert('Selecione as cotações para concatenar e enviar!');
		}	
		
	}
</script>
<?php

}//fachando o teste se está logado!!
?>
<style type="text/css">		
	    @import "../dijit/themes/tundra/tundra.css";		
		@import "../dojox/grid/resources/Grid.css";		
		@import "../dojox/grid/resources/tundraGrid.css";		
	</style>
</head>

<body class="tundra">
 <div id="loader"><img src="img/loading.gif" />Carregando</div>
 <div id="modal"></div>
 <h1>Gestão de cotações automáticas</h1>
 <?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
 if ($_SESSION['iduser'] > 0){
   //agora vamos ver qual o nível deste usuário
   require_once("../classes/Connection.php");
   $buscaN = new Connection();
   
   $nivel = $buscaN->buscaNivel($_SESSION['iduser']);//executando a função da classe Connection, que retorna o nível deste usuário
 
   if ($nivel == 0){	
    ?>
				   <div id="form" class="dialog">
					  <form name="palavras" method="post" dojoType="dijit.form.Form">
					     <label>Loja</label><input dojoType="dijit.form.TextBox" name="loja" id="loja" />
						 <label>Data</label><input dojoType="dijit.form.DateTextBox" name="data" id="data" />
					  </form>
					  <div id="btns2">
							<button dojoType="dijit.form.Button" onClick="cadastraPop()" id="new">Novo</button>
							<button dojoType="dijit.form.Button" onclick="regras();" id="limpa">Regras da cotação/pedido</button>
							<button dojoType="dijit.form.Button" onclick="concatenar();" id="concat">Concatenar e Enviar</button>
					  </div>
				   </div> <!-- fecha o formulário -->			   
				   <div id="gridNode" jsId="grid1"></div>
  <?php
    } //fecha o if do nível de usuário
	 else{
	    echo "Usuário sem permissão para acessar esta função!";
	 }
 }else{
 	echo "Usuário não está logado!";
 }	 
   ?>		
</body>
</html>
