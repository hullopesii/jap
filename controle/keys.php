<?PHP
	// para controle de seguran�a, utilizaremos vari�veis de sess�o, isto aqui est� inicializando a sess�o
	session_start("login");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Gest�o de palavras chaves - keywords</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
<script type="text/javascript" src="script/prototype.js"></script>
<?php
     //checando se tem alguem logado, �ltima tentativa de seguran�a. se n�o tiver logado, daqui n�o passa
if ($_SESSION['iduser'] > 0){
?>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
  dojo.require("dijit.form.Button");
  dojo.require("dijit.form.TextBox");
  dojo.require("dojox.form.DropDownSelect");
  dojo.require("dojox.grid.DataGrid");
  dojo.require("dojo.data.ItemFileWriteStore");
   
  idUserLogado = "<?php echo $_SESSION['iduser'] ?>";

  
  var grid = null; 
  dojo.addOnLoad(function(){	
		
		 var jsonStore = new dojo.data.ItemFileWriteStore({ url: "json/keysHome.php" , clearOnClose: true});  
		  
	     var layout= [	
		 		{ field: "part_num", width: "10%", name: "id", editable:false },
				{ field: "palavra", width: "30%", name: "Palavra", editable: true }, 
				{ field: "pagina", width: "30%", name: "P�gina", editable: true, type: dojox.grid.cells.Select, options: [ 'Home', 'Contato', 'A empresa', 'Produtos']  },
				{ field: "tipo", width: "10%", name: "Tipo", editable: true, type: dojox.grid.cells.Select, options: [ 'Meta Tags', 'Rodape' ], values: ['meta', 'rodap�'] }
				];
		 
		grid = new dojox.grid.DataGrid({
						Id: "grid1",
						singleClickEdit: false,
						store: jsonStore,
						structure: layout,
						loadingMessage:'Carregando palavras',
						onApplyCellEdit: editEduRow,
						rowsPerPage: 20	},
						 'gridNode');
		 grid.store.close();
		 //grid.onApplyEdit(editando);
		grid.startup();
		dojo.connect(grid, "onKeyPress", clique);
	});
	
	function clique(tecla){   			
	  if(tecla.keyCode == 46){    //se a tecla foi o del, vamos deletar o item selecionado
       if (confirm('Deseja realmente excluir esta palavra?')){
		 var identificador = pegandoId(grid.selection.getSelected());
		 
			 var url = 'excluindoPalavras.php?id='+identificador+'&iduser='+idUserLogado;
			 
			 retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
		     
		}//fecha confirm 
		 
	  }
	}
	
	function pegandoId(linhaSelecionada){
	  var arrayform = new Array();
	  if(linhaSelecionada.length){
            dojo.forEach(linhaSelecionada, function(selectedItem) {
                if(selectedItem !== null) {
					var ii = 0;					
                    dojo.forEach(grid.store.getAttributes(selectedItem), function(attribute) {
                        var value = grid.store.getValues(selectedItem, attribute);
                        
						arrayform[ii] = value;
						ii++; 
                    }); // end forEach
                } // end if
            }); // end forEach
        } // end if
		
		return arrayform[0];
	}
	
	function editEduRow(txt, index, campo){
		
		var identificador = pegandoId(grid.selection.getSelected());
		
		//utilzando m�dulo ajax do prototype
		var url = 'alterandoPalavras.php?id='+identificador+'&campo='+campo+'&txt='+txt;//variavel que vai mandar os dados via get 
		//blz, criamos a variavel url, que contem o nome da p�gina php que vai fazer altera��o no banco de dados
		
		//aqui mandamos via ajax, acionando a p�gina definida na "url"
		retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
	 
	}
	
	function carregando(){
		$('loader').style.visibility='visible';	
		$('modal').style.visibility='visible';
	}
	function carregado(){
		$('loader').style.visibility='hidden';	
		$('modal').style.visibility='hidden';
	}
	function falha(){
	  alert('falha no carregamento');
	  carregado();
	}
	function resposta(resp){
	    carregado();
		var json = resp.responseText;//pegando o texto retornado pela p�gina de altera��o
		if (json == 'ok'){
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/keysHome.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		}else{
		  alert('Ocorreu algum erro que impossibilitou a altera��o, por favor, tente novamente!');
		}			
	}
	function cadastraPalavra(){

	  palavra  = $('palavra').value;
	  pagina = dijit.byId('pagina').attr('value');
	  tipo    = dijit.byId('tipo').attr('value');
	  	
	  checagem  = true;
	  
	  if (palavra == ''){

		alert('Informe corretamente a palavra');
		$('palavra').focus();
		checagem = false;
	  }   

	  if(checagem){
		var url2 = 'cadastrandoPalavras.php?palavra='+palavra+'&pagina='+pagina+'&tipo='+tipo+'&idUser='+idUserLogado;
		
		retorno2 = new Ajax.Request(url2, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta2});
	  }
	}
	
	function resposta2(resp2){
	    var json2 = resp2.responseText;
		carregado();

		if (json2 == 'ok'){ 
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/keysHome.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		  document.palavras.reset();
		}else{
		  alert('Devido alguma falha, n�o foi efetivado o cadastro da keyword!');
		}  
			  
	}
</script>
<?php

}//fachando o teste se est� logado!!
?>
<style type="text/css">		
	    @import "../dijit/themes/tundra/tundra.css";		
		@import "../dojox/grid/resources/Grid.css";		
		@import "../dojox/grid/resources/tundraGrid.css";		
	</style>
</head>

<body class="tundra">
 <div id="loader"><img src="img/loading.gif" />Carregando</div>
 <div id="modal"></div>
 <h1>Gest�o de palavras chaves -  keywords</h1>
 <?php
     //checando se tem alguem logado, �ltima tentativa de seguran�a. se n�o tiver logado, daqui n�o passa
 if ($_SESSION['iduser'] > 0){
   //agora vamos ver qual o n�vel deste usu�rio
   require_once("../classes/Connection.php");
   $buscaN = new Connection();
   
   $nivel = $buscaN->buscaNivel($_SESSION['iduser']);//executando a fun��o da classe Connection, que retorna o n�vel deste usu�rio
 
   if ($nivel == 0){	
    ?>
				   <div id="form" class="dialog">
					  <form name="palavras" method="post" dojoType="dijit.form.Form">
						 <label>Palavra</label><input dojoType="dijit.form.TextBox" name="palavra" id="palavra" />
						 <label>P�gina</label><select dojoType="dojox.form.DropDownSelect" name="pagina" id="pagina">
												 <option value="Home">Home</option>
												 <option value="Contato">Contato</option>
												 <option value="A empresa">A empresa</option>
												 <option value="Produtos">Produtos</option>
											 </select>
						 
						 <label>Tipo</label><select dojoType="dojox.form.DropDownSelect" name="tipo" id="tipo">
												 <option value="meta">Meta tags</option>
											 </select>
						 <br />
					  </form>
					  <div id="btns2">
							<button dojoType="dijit.form.Button" onClick="cadastraPalavra()" id="new">Novo</button>
							<button dojoType="dijit.form.Button" onclick="document.palavras.reset();" id="limpa">Limpar</button>
					  </div>
				   </div> <!-- fecha o formul�rio -->			   
				   <div id="gridNode" jsId="grid1"></div>
  <?php
    } //fecha o if do n�vel de usu�rio
	 else{
	    echo "Usu�rio sem permiss�o para acessar esta fun��o!";
	 }
 }else{
 	echo "Usu�rio n�o est� logado!";
 }	 
   ?>		
</body>
</html>
