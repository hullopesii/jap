<?PHP
	session_start("login");
    ob_start();

    header("Expires: Fri, 25 Dec 1980 00:00:00 GMT");
    header("Last-Modified: " . gmdate('D, d M Y H:i:s') . 'GMT');
	header('Cache-Control: no-cache, must-revalidate');
	header('Pragma: no-cache');
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Gerenciamento web 2.0 Pandorati</title>
<!-- carregamento dos arquivos externos -->
 <link href="css.css" rel="stylesheet" type="text/css" />

 <script type="text/javascript" src="script/prototype.js"></script>
 <script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
 <script type="text/javascript" src="script/defs.js" djConfig="parseOnLoad:true, isDebug: false"></script>



 <script type="text/javascript">
	dojo.require("dijit.form.TextBox"); 
	dojo.require("dijit.form.Button");
	dojo.require("dijit.form.Form");	
	var argumentos = new Array();	
	dojo.addOnLoad(function(){  inicializa();});	
	function inicializa(){		
		//criando os argumentos do grid
         argumentos = define();
		dojo.connect(dijit.byId("login"), "onClick", clicaLogin);		
	}
	function clicaLogin(){			
			formulario = document.logando;
			checa = true;
			if (formulario.nome.value == ''){
			  alert('Informe o nome de usu�rio!');
			  formulario.nome.focus();
			  checa = false;
			}else{
			  if ($('password').value == ''){
			      alert('Digite a senha!');
				  $('password').focus();
				  checa = false;
			  }	  
			}
			var url = argumentos[2]+'?nm='+formulario.nome.value+'&pass='+formulario.senha.value;
			if (checa){
				var retornoAjax = new Ajax.Request(url, {method: 'get',onLoading:carregandoLogin, onFailure: falhaLogin, onSuccess: respostaLogin});
			}			
	}
	
	function carregandoLogin(){
		$('logando').style.visibility = 'hidden';
		$('loader3').style.visibility = 'visible';
	}
	function carregado(){
	    $('logando').style.visibility = 'visible';
		$('loader3').style.visibility = 'hidden';
	}
	function falhaLogin(){
		alert("Falha no login");
	}

	function respostaLogin(resp){
	  if (resp.responseText !== 'tentativas esgotadas'){ 
		  if(resp.responseText !== 'erro'){
			sessao = resp.responseText;
			var url = "index2.php";
			document.location = url;
		  }else{
		    carregado();
			$('msgLogin').style.visibility='visible';
			$('logando').reset();
			$('logando').nome.focus();
			//seu cavalo, 5 tentativas e bloqueio seu ip
		   }
	  }else{
	    alert('N�mero de tentativas excedidas, seu ip foi bloqueado! Contacte o administrador');
	  }	   
	}
</script>


<style type="text/css">
    @import "../dijit/themes/soria/soria.css";
</style>

<!-- fim do carregamento dos arquivos externos --> 
</head>

<body class="soria">
 <center>
   <div id="qdlogin">
     <div id="loader3" style="visibility:hidden"><img src="img/loading.gif" />Carregando</div>
	 <form dojoType="dijit.form.Form" name="logando" method="post" id="logando" encType="multipart/form-data" onsubmit=" clicaLogin(); return false;">

	    <table width="100%" border="0" cellspacing="10" cellpadding="0">
		  <tr>
			<td width="25%" align="right" style="color:#FFFFFF">Usu�rio:</td>
			<td width="75%"><input dojoType="dijit.form.TextBox" name="nome" id="nomeUser" /></td>
		  </tr>
		  <tr>
			<td align="right" style="color:#FFFFFF">Senha:</td>
			<td><input dojoType="dijit.form.TextBox" name="senha" id="password" type="password" /></td>
		  </tr>
		</table>
		    <button dojoType="dijit.form.Button"  id="login">Logar</button>
			<div id="msgLogin">Login incorreto<br />esta tentativa foi gravada</div>
		   </form>	
		</div>
   </div>
  </center>
</body>
</html>