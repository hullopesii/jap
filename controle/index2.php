<?PHP
	//session_start();
    //ob_start();
    
    session_start();
  if (empty($_SESSION['iduser'])){
	  $_SESSION['iduser'] = 0;
	   header("Location:index.php");
	}
	
    header("Expires: Fri, 25 Dec 1980 00:00:00 GMT");
    header("Last-Modified: " . gmdate('D, d M Y H:i:s') . 'GMT');
	header('Cache-Control: no-cache, must-revalidate');
	header('Pragma: no-cache');
	
	if (empty($_SESSION['iduser'])){
	  $_SESSION['iduser'] = 0;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta charset="UTF-8">
<title>Gerenciamento web 2.0 Pandorati</title>
<!-- carregamento dos arquivos externos -->
 <link href="css.css" rel="stylesheet" type="text/css" />

 <script type="text/javascript" src="script/prototype.js"></script>
 <script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
  <script src="script/home.js" type="text/javascript" ></script>
  <script type="text/javascript" src="script/defs.js"></script>

 <script type="text/javascript">

   	dojo.require("dijit.Dialog");
	dojo.require("dijit.form.TextBox"); 
	dojo.require("dijit.form.Button");
	dojo.require("dijit.form.Form");
	
	var argumentos = new Array();
	
	//Event.observe(window, 'load', inicializa, false);
	dojo.addOnLoad(function(){  inicializa();});
	
	var sessao=0;
	sessao = "<?php echo $_SESSION['iduser'] ?>";
	
	function inicializa(){
		
		
		//criando os argumentos do grid
         argumentos = define();
		
		if (sessao == 0){
		  var formDlg = dijit.byId("formDialog");
		  formDlg.show();
	
		  limpacampo = document.logando;
		  limpacampo.nome.value='';
		  limpacampo.senha.value='';
		}
		dojo.connect(dijit.byId("login"), "onClick", clicaLogin);
		
	}
	function clicaLogin(){
	
			formulario = document.logando;
			checa = true;
			if (formulario.nome.value == ''){
			  alert('Informe o nome de usuário!');
			  formulario.nome.focus();
			  checa = false;
			}else{
			  if ($('password').value == ''){
			      alert('Digite a senha!');
				  $('password').focus();
				  checa = false;
			  }	  
			}
			var url = argumentos[2]+'?nm='+formulario.nome.value+'&pass='+formulario.senha.value;
			if (checa){
				var retornoAjax = new Ajax.Request(url, {method: 'get',onLoading:carregandoLogin, onFailure: falhaLogin, onSuccess: respostaLogin});
			}			
	}
	
	function carregandoLogin(){
		$('logando').style.visibility = 'hidden';
		$('loader2').style.visibility = 'visible';
	}
	function carregado(){
	    $('logando').style.visibility = 'visible';
		$('loader2').style.visibility = 'hidden';
	}
	function falhaLogin(){
		alert("Falha no login");
	}

	function respostaLogin(resp){
	  carregado();	
	  if (resp.responseText !== 'tentativas esgotadas'){ 
		  if(resp.responseText !== 'erro'){
			sessao = resp.responseText;
			var formDlg = dijit.byId("formDialog");
			formDlg.hide();
		  }else{
			$('msgLogin').style.visibility='visible';
			$('logando').reset();
			$('logando').nome.focus();
			//seu cavalo, 5 tentativas e bloqueio seu ip
		   }
	  }else{
	    alert('Número de tentativas excedidas, seu ip foi bloqueado! Contacte o administrador');
	  }	   
	}
</script>


<style type="text/css">
    @import "../dijit/themes/soria/soria.css";
</style>

<!-- fim do carregamento dos arquivos externos --> 
</head>

<body class="soria">
 <center>
  <!-- início do corpo do sistema -->
  <img src="img/abaTopo.png" />
  <div id="corpo">
    <div id="topo">
	<div id="logo"></div>
	  <div id="menu">
		<span>
		  <a href="#" id="usuarios"><img src="img/users.png" border="0" />Usuários</a>
		  <div id="span1">
			<a href="#" onclick="logoff()">Logoff</a>
			<a href="#" onclick="chamapagina('cadastrouser.php')">Cadastro</a>
			<a href="#" onclick="chamapagina('alterandoSenha.php')">Mudar Senha</a>
		  </div>
		</span> 
		<span>
		  <a href="#" style="visibility: hidden;"><img src="img/config.png" border="0" />Configurações</a>
		  <div id="span2">
			<!--<a href="#" onclick="chamapagina('keys.php')" title="Palavras chaves do site">Key words</a>
			<a href="#" onclick="chamapagina('popups.php')">Pop Ups</a>
			<a href="#" onclick="chamapagina('atendentes.php')">-->Atendentes Chat</a>
		  </div>
		</span>
		<span>
		  <a href="#"><img src="img/mundo.png" border="0" />Cadastro</a>
		  <div id="span3">
			<!--<a href="#" onclick="chamapagina('categorias.php')">Categorias</a>
			<a href="#" onclick="chamapagina('marcas.php')">Marcas</a>
			<a href="#" onclick="chamapagina('produtos.php')">Produtos</a>
			<a href="#" onclick="chamapagina('emails.php')">Emails</a> -->
			<a href="#" onclick="chamapagina('fornecedores.php')">Fornecedores</a>
			<a href="#" onclick="chamapagina('cotacoes.php')">Listas de cotações</a>
		  </div>
		</span>
	  </div><!-- fecha o menu -->
    </div><!-- fecha o topo -->	
   <div id="loader"><img src="img/loading.gif" />Carregando</div>
   <div id="centro">
   <iframe id="iframecentro" src="home.php" frameborder="0"></iframe>
   
   <!-- início da janelinha de diálogo para login -->
   <div id="formDialog" dojoType="dijit.Dialog" class="mostrador1" title="Login">
        <div class="pane1" id="panel">
		   <div id="loader2" style="visibility:hidden"><img src="img/loading.gif" />Carregando</div>
		   <form dojoType="dijit.form.Form" name="logando" method="get" id="logando" encType="multipart/form-data" onsubmit=" clicaLogin(); return false;">
			<label>Usuário:</label><input dojoType="dijit.form.TextBox" name="nome" id="nomeUser" />
			<label>Senha:</label><input dojoType="dijit.form.TextBox" name="senha" id="password" type="password" />
			<br /><label id="erro" style="color:#990000"></label><br />
		    <button dojoType="dijit.form.Button"  id="login">Logar</button>
			<div id="msgLogin">Login incorreto<br />esta tentativa foi gravada</div>
		   </form>	
		</div>
	</div>
	    
   </div><!-- fecha o centro -->
  </div> 
  <!-- fim do corpo do sistema -->
 </center>
</body>
</html>