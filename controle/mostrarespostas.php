 <?PHP
 /*
  session_start();
  
  if (empty($_SESSION['iduser'])){
	  $_SESSION['iduser'] = 0;
	   header("Location:index.php");
	}
	*/
	
	$id = $_GET['id'];
	
	require_once("../classes/Connection.php");
	$query = new Connection();
	
	$resp = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE `idCotacao`={$id}");
	$row = $resp->fetch_assoc();
	
	require_once("../classes/Funcoes.php");
	$funcoes = new Funcoes();
	
	require_once("../classes/Listagem.php");
	$listagem = new Listagem();
	
	$listagem->contaGanhos($id);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Respostas dos fornecedores</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script src="../scripts/jquery.1.5.2.min.js"></script>
    <script src="../scripts/jquery.tablesorter.min.js"></script>
    <script src="../scripts/jquery.tablesorter.pager.js"></script>
<style>
  .porta1 a{ font-size: 15px; }
  table{
  width:98%;
  border:1px solid #ccc;
  font-size:12px;
}
.fornecedorResp{width:200px; height:140px; float:left; background:#CCCCCC; margin:5px; padding:2px; text-align:center;}

a.linkr:link{text-decoration: none; color: #2F4F4F;}
a.linkr:hover{text-decoration: underline; color:black;}

table thead th{
  background-color:#ad2525;
  padding:6px;
  color:#fff;
  text-align:center;
  font-size:12px;
}

table thead th.header{
  cursor:pointer;
}

table tbody td{
  padding:6px;
  text-align:center;
  color:#333;
}

table tbody tr.odd td{
  background-color:#ffffcc;
}

table tbody tr.hover td{
  background-color:#a9d0f5;
}

table tbody tr.selected td{
  background-color:#a9f5a9!important;
}

.pagedisplay{
	width:40px;
	text-align:center;
	border:none;
	background-color:#f2f2f2;
	font-weight:900;
}

#pager span{
	font-weight:900;
	display:inline-block;
	margin:0 0 0 20px;
	color:#666;
	float:right;
}

#pager form{
	text-align:left;
	padding:10px;
	width:580px;
}
a img{
	border:none;
}

form{
  background-color:#f2f2f2;
  width:600px;
  margin:10px 0 10px 0;
  text-align:center;
}

form p{
  padding:12px;
}

form p label{
  color:#333;
  font-weight:900;
}

form input{
  padding:6px;
  border:1px solid #ccc;
  width:300px;
}
</style>
</head>

<body>
<h1>Respostas das cotações - Cotação código <?php echo $row['idCotacao']." data ". $funcoes->trataData($row['data']); ?></h1>
  <div class="porta1">
  <?php
      function respCotacoes($idF){
	       $resp = $query->getConsulta2("SELECT * FROM WHERE `respitemcotacao` WHERE (`idFornecedor` = {$idF} and `idCotacao`= {$id})");
		   $html = '';
		   
		   while ($row = $resp->fetch_assoc()){
		      $html.= $resp->num_rows;		   
		   }
		   return $html;
	  }
      //buscando os fornecedores que responderam
	  $respFornecedores = $query->getConsulta2("SELECT * FROM `respitemcotacao` inner join fornecedores on fornecedores.idFornecedor = respitemcotacao.idFornecedor WHERE respitemcotacao.idCotacao={$id} group by respitemcotacao.idFornecedor");

	  $arPrecos = $listagem->arrPrecos;


	  
	  if (count($arPrecos) > 0){
	  
			  while ($rowFornecedores = $respFornecedores->fetch_assoc()){
				  $resp = $query->getConsulta2("SELECT * FROM `respitemcotacao` WHERE (`idFornecedor` = ".$rowFornecedores['idFornecedor']." and `idCotacao`= {$id})");
		
				  $idFornecedor = $rowFornecedores['idFornecedor'];
				  $arFornecedor = $arPrecos[$idFornecedor];
				  echo "<div class='fornecedorResp'>";
				  echo "<a href='mostrarespostasByfornecedor.php?idF=".$idFornecedor."&id=".$id."' title='Clique para ver a relação dos produtos'>".$rowFornecedores['fornecedor'];
				  echo "<p>".$resp->num_rows." Produtos Respondidos</p>";
				  echo "<p>Venceu em ".count($arFornecedor)." produtos</p>";
				  echo "</a>";
				  echo "<a href='mostrarespostasByfornecedorDetalhado.php?idF=".$idFornecedor."&id=".$id."' title='Clique para ver a relação de todos os produtos respondidos por este fornecedor' class='linkr'>";
				  echo "Todas respostas</a>";
				  echo "</div>";
			  }
		}else{
		   echo "<h3>Nenhuma cotação respondida ainda</h3>";
		}	  
  ?>
  </div>
  
  <table width="100%" border="0" cellspacing="3" cellpadding="3">
    <thead>
	  <tr>
		<td width="5%" bgcolor="#CCCCCC">C&oacute;digo</td>
		<td width="40%" bgcolor="#CCCCCC">Descri&ccedil;&atilde;o</td>
		<td width="10%" bgcolor="#CCCCCC">Marca</td>
		<td width="5%" bgcolor="#CCCCCC">Qtd Cotada</td>
		<td width="8%" bgcolor="#CCCCCC">Mínimo</td>
		<td width="8%" bgcolor="#CCCCCC">Máximo</td>
		<td width="8%" bgcolor="#CCCCCC">Valores</td>
	  </tr>
	 </thead> 
  <tbody>
  <?php
      function pegaNomeF($valor, $idP){
	      $query = new Connection();
		  if ($valor > 0){
			  $r = $query->getConsulta2("SELECT * FROM `respitemcotacao` inner join fornecedores on fornecedores.idFornecedor = respitemcotacao.idFornecedor WHERE (respitemcotacao.preco= {$valor} and respitemcotacao.idItemCotacao={$idP})");
			   $html = '';
			  while ($row = $r->fetch_assoc()){
			    $html.= $row['fornecedor'].',';
			  }
			  return substr($html, 0, -1);
		   }	   
	  }
	  function todosValores($idI,$id){
	  	  $query = new Connection();
	      $r = $query->getConsulta2("SELECT respitemcotacao.preco,fornecedores.fornecedor FROM `respitemcotacao` inner join fornecedores on fornecedores.idFornecedor = respitemcotacao.idFornecedor where idCotacao={$id} and idItemCotacao={$idI} order by preco");

	      $html = "";
	      while($row = $r->fetch_assoc()){
	      	$html.="\n&#013;".$row['fornecedor'] . " R$ ". number_format($row['preco'], 2, ",", ".");
	      }
	      return $html;
	  }
      function pegaMinimo($idp){
	      $query = new Connection();
	      $r = $query->getConsulta2("SELECT *, MIN(preco) as 'min',  MAX(preco) as 'max' FROM `respitemcotacao` WHERE idItemCotacao = {$idp} ");
		  $ar = array();
		  if ($r->num_rows > 0){
		     $rw = $r->fetch_assoc();
		     $ar['min'] = $rw['min'];
			 $ar['max'] = $rw['max'];
		  }
		  return $ar;
	  }
      $res = $query->getConsulta2("SELECT * FROM `itenscotacoes` WHERE `idCotacao` = {$id}");
	  $html = '';
	  while ($r = $res->fetch_assoc()){
	     $mima = pegaMinimo($r['idItem']);
		 //pegando nome do fornecedor
		 $title = pegaNomeF($mima['min'], $r['idItem']);
		 $title2= pegaNomeF($mima['max'], $r['idItem']);
		 $title3= todosValores($r['idItem'],$id);
		 
		 $html.=' <tr>
					<td>'.$r['codigo'].'</td>
					<td  style="text-align:left">'.$r['descricao'].'</td>
					<td  style="text-align:left">'.$r['referencia'].'</td>
					<td>'.$r['qtdCotada'].'</td>
					<td title="'.$title.'">'.number_format($mima['min'], 2, ",", ".").'</td>
					<td title="'.$title2.'">'.number_format($mima['max'], 2, ",", ".").'</td>
					<td title="'.$title3.'">R$</td>
				  </tr>';
	  }
	  echo $html;
  ?>
  </tbody>
</table>
<div id="pager" class="pager">
    	<form>
				<span>
					Exibir <select class="pagesize">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option  value="40">40</option>
					</select> registros
				</span>

				<img src="img/first.png" class="first"/>
    		<img src="img/prev.png" class="prev"/>
    		<input type="text" class="pagedisplay"/>
    		<img src="img/next.png" class="next"/>
    		<img src="img/last.png" class="last"/>
    	</form>
    </div>

<script>
    $(function(){
      
      $('table > tbody > tr:odd').addClass('odd');
      
      $('table > tbody > tr').hover(function(){
        $(this).toggleClass('hover');
      });
      
      $('#marcar-todos').click(function(){
        $('table > tbody > tr > td > :checkbox')
          .attr('checked', $(this).is(':checked'))
          .trigger('change');
      });
      
      $('table > tbody > tr > td > :checkbox').bind('click change', function(){
        var tr = $(this).parent().parent();
        if($(this).is(':checked')) $(tr).addClass('selected');
        else $(tr).removeClass('selected');
      });
      
      $('form').submit(function(e){ e.preventDefault(); });
      
      $('#pesquisar').keydown(function(){
        var encontrou = false;
        var termo = $(this).val().toLowerCase();
        $('table > tbody > tr').each(function(){
          $(this).find('td').each(function(){
            if($(this).text().toLowerCase().indexOf(termo) > -1) encontrou = true;
          });
          if(!encontrou) $(this).hide();
          else $(this).show();
          encontrou = false;
        });
      });
      
      $("table") 
        .tablesorter({
          dateFormat: 'uk',
          headers: {
            0: {
              sorter: false
            },
            5: {
              sorter: false
            }
          }
        }) 
        .tablesorterPager({container: $("#pager")})
        .bind('sortEnd', function(){
          $('table > tbody > tr').removeClass('odd');
          $('table > tbody > tr:odd').addClass('odd');
        });
      
    });
    </script>

</body>
</html>
