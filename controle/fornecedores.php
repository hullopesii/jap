<?PHP
	// para controle de segurança, utilizaremos variáveis de sessão, isto aqui está inicializando a sessão
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Marcas de produtos</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
<script type="text/javascript" src="script/prototype.js"></script>
<?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
if ($_SESSION['iduser'] > 0){
?>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
  dojo.require("dijit.form.Button");
  dojo.require("dijit.form.TextBox");
  dojo.require("dojox.form.DropDownSelect");
  dojo.require("dojox.grid.DataGrid");
  dojo.require("dojo.data.ItemFileWriteStore");
   
  idUserLogado = "<?php echo $_SESSION['iduser'] ?>";

  
  var grid = null; 
  dojo.addOnLoad(function(){	
		
		 var jsonStore = new dojo.data.ItemFileWriteStore({ url: "json/fornecedores.php" , clearOnClose: true});  
		  
	     var layout= [	
		 		{ field: "part_num", width: "10%", name: "id", editable:false },
				{ field: "fornecedor", width: "30%", name: "Fornecedor", editable: true },
				{ field: "fone", width: "20%", name: "Fone", editable: true },
				{ field: "email", width: "20%", name: "E-Mail", editable: true }
				];
		 
		grid = new dojox.grid.DataGrid({
						Id: "grid1",
						singleClickEdit: false,
						store: jsonStore,
						structure: layout,
						loadingMessage:'Carregando categorias',
						onApplyCellEdit: editEduRow,
						rowsPerPage: 20	},
						 'gridNode');
		 grid.store.close();
		 //grid.onApplyEdit(editando);
		grid.startup();
		dojo.connect(grid, "onKeyPress", clique);
	});
	
	function clique(tecla){   			
	  if(tecla.keyCode == 46){    //se a tecla foi o del, vamos deletar o item selecionado
       if (confirm('Deseja realmente excluir este fornecedor?')){
		 var identificador = pegandoId(grid.selection.getSelected());
		 
			 var url = 'excluindoFornecedores.php?id='+identificador+'&iduser='+idUserLogado;
			 
			 retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
		     
		}//fecha confirm 
		 
	  }
	}
	
	function pegandoId(linhaSelecionada){
	  var arrayform = new Array();
	  if(linhaSelecionada.length){
            dojo.forEach(linhaSelecionada, function(selectedItem) {
                if(selectedItem !== null) {
					var ii = 0;					
                    dojo.forEach(grid.store.getAttributes(selectedItem), function(attribute) {
                        var value = grid.store.getValues(selectedItem, attribute);
                        
						arrayform[ii] = value;
						ii++; 
                    }); // end forEach
                } // end if
            }); // end forEach
        } // end if
		
		return arrayform[0];
	}
	
	function editEduRow(txt, index, campo){
		
		var identificador = pegandoId(grid.selection.getSelected());
		
		var url = 'alterandoFornecedor.php?id='+identificador+'&campo='+campo+'&txt='+txt;//variavel que vai mandar os dados via get 

		retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
	 
	}
	
	function carregando(){
		$('loader').style.visibility='visible';	
		$('modal').style.visibility='visible';
	}
	function carregado(){
		$('loader').style.visibility='hidden';	
		$('modal').style.visibility='hidden';
	}
	function falha(){
	  alert('falha no carregamento');
	  carregado();
	}
	function resposta(resp){
	    carregado();
		var json = resp.responseText;//pegando o texto retornado pela página de alteração
		if (json == 'ok'){
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/fornecedores.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		}else{
		  alert('Ocorreu algum erro que impossibilitou a alteração, por favor, tente novamente!');
		}			
	}
	function cadastra(){

	  fornecedor  = $('fornecedor').value;
	  fone		  = $('fone').value;
	  email  = $('email').value;
	  	
	  checagem  = true;
	  
	  if (fornecedor == ''){
		alert('Informe corretamente a marca');
		$('marca').focus();
		checagem = false;
	  }   

	  if(checagem){
		var url2 = 'cadastrandoFornecedores.php?fornecedor='+fornecedor+'&idUser='+idUserLogado+'&fone='+fone+'&email='+email;
		 
		retorno2 = new Ajax.Request(url2, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta2});
	  }
	}
	
	function resposta2(resp2){
	    var json2 = resp2.responseText;
		carregado();

		if (json2 == 'ok'){ 
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/fornecedores.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		  document.palavras.reset();
		}else{
		  alert('Devido falha, não foi efetivado o cadastro da marca!');
		}  
			  
	}
</script>
<?php

}//fachando o teste se está logado!!
?>
<style type="text/css">		
	    @import "../dijit/themes/tundra/tundra.css";		
		@import "../dojox/grid/resources/Grid.css";		
		@import "../dojox/grid/resources/tundraGrid.css";		
	</style>
</head>

<body class="tundra">
 <div id="loader"><img src="img/loading.gif" />Carregando</div>
 <div id="modal"></div>
 <h1>Gestão das Fornecedores de produtos</h1>
 <?php
     //checando se tem alguem logado, última tentativa de segurança. se não tiver logado, daqui não passa
 if ($_SESSION['iduser'] > 0){
   //agora vamos ver qual o nível deste usuário
   require_once("../classes/Connection.php");
   $buscaN = new Connection();
   
   $nivel = $buscaN->buscaNivel($_SESSION['iduser']);//executando a função da classe Connection, que retorna o nível deste usuário
 
   if ($nivel == 0){	
    ?>
				   <div id="form" class="dialog">
					  <form name="palavras" method="post" dojoType="dijit.form.Form">
						 <label>Fornecedor</label>
						 <input dojoType="dijit.form.TextBox" name="fornecedor" id="fornecedor" />			 						 
						 <label>Fone</label>
						 <input dojoType="dijit.form.TextBox" name="fone" id="fone" />
						 <br />
						 <label>E-Mail</label>
						 <input dojoType="dijit.form.TextBox" name="email" id="email" />
					  </form>
					  <div id="btns2">
							<button dojoType="dijit.form.Button" onClick="cadastra()" id="new">Novo</button>
							<button dojoType="dijit.form.Button" onclick="document.palavras.reset();" id="limpa">Limpar</button>
					  </div>
				   </div> <!-- fecha o formulário -->			   
				   <div id="gridNode" jsId="grid1"></div>
  <?php
    } //fecha o if do nível de usuário
	 else{
	    echo "Usuário sem permissão para acessar esta função!";
	 }
 }else{
 	echo "Usuário não está logado!";
 }	 
   ?>		
</body>
</html>
