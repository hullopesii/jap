<?PHP
	session_start("login");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Gest�o de usu�rios</title>
<link href="css2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
<script type="text/javascript" src="script/prototype.js"></script>
<?php
if ($_SESSION['iduser'] > 0){
	require_once("../AtendNet/amfphp/services/classes/Connection.php");
	
	$query = new Connection();
	
	$resp = $query->getConsulta2("SELECT * FROM departamento ORDER BY `departamento`");

	$i = 1;
    $linha = $resp->num_rows;
   
	$options = "[";
	$values  = "[";
	while ($row = $resp->fetch_assoc() ){
		$options .= "'".$row["departamento"]."'";
		$values .= "'".$row["idDepartamento"]."'";
		if ($i == $linha){
			  $options.='';
			}
			else{
			  $options.=',';
			  $values.=',';
			}
			$i++;  
	}
	$options .="]";
	$values  .="]";
	
?>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
  dojo.require("dijit.form.Button");
  dojo.require("dijit.form.TextBox");
  dojo.require("dojox.grid.DataGrid");
  dojo.require("dojo.data.ItemFileWriteStore");
  dojo.require("dijit.form.FilteringSelect");
   
  idUserLogado = "<?php echo $_SESSION['iduser'] ?>";
  var idDepartamento = null;  
  var grid = null; 
  dojo.addOnLoad(function(){			
		 var jsonStore = new dojo.data.ItemFileWriteStore({ url: "json/atendentes.php" , clearOnClose: true});  
		  
	     var layout= [	
		 		{ field: "part_num", width: "10%", name: "id", editable:false },
				{ field: "nmAtendente", width: "30%", name: "Nome", editable: true }, 
				{ field: "user", width: "30%", name: "User Name", editable: true },
				{ field: "idDepartamento", width: "10%", name: "Departamento", editable: true, type: dojox.grid.cells.Select, options: <?php echo $options; ?>, values: <?php echo $values; ?> },
				{ field: "status", width: "100px", name: "Status", editable: true, type: dojox.grid.cells.Select, options: ['Ativo', 'Ausente'], values: ['Ativo', 'Ausente'] }
				];
		 
		grid = new dojox.grid.DataGrid({
						Id: "grid1",
						singleClickEdit: false,
						store: jsonStore,
						structure: layout,
						loadingMessage:'Carregando',
						onApplyCellEdit: editEduRow,
						rowsPerPage: 20	},
						 'gridNode');
		 grid.store.close();
		 
		grid.startup();
		dojo.connect(grid, "onKeyPress", clique);
		//drop down departamentos
		var jsonStore2 = new dojo.data.ItemFileReadStore({ url: "json/mostraDepartamentos.php"}); 

		var filteringSelect = new dijit.form.FilteringSelect({
						id : "selectDepartamento",
						store: jsonStore2,
						searchAttr: "departamento"					
					}, "selectDepartamento");
				
		dojo.connect(filteringSelect, "onChange", function(){
		   idDepartamento = dijit.byId('selectDepartamento').attr('value');
		});
	});
	
	function clique(tecla){   			
	  if(tecla.keyCode == 46){
       if (confirm('Deseja realmente excluir este atendente?')){
	         var identificador = pegandoId(grid.selection.getSelected());
			 var url = 'excluindoAtendente.php?id='+identificador+'&iduser='+idUserLogado;
			 retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});
		}	 
	  }
	}
	
	function pegandoId(linhaSelecionada){
	  var arrayform = new Array();
	  if(linhaSelecionada.length){
            dojo.forEach(linhaSelecionada, function(selectedItem) {
                if(selectedItem !== null) {
					var ii = 0;					
                    dojo.forEach(grid.store.getAttributes(selectedItem), function(attribute) {
                        var value = grid.store.getValues(selectedItem, attribute);                        
						arrayform[ii] = value;
						ii++; 
                    }); // end forEach
                } // end if
            }); // end forEach
        } // end if
		
		return arrayform[0];
	}
	
	function editEduRow(txt, index, campo){
		var identificador = pegandoId(grid.selection.getSelected());
		var url = 'alterandoAtendente.php?id='+identificador+'&campo='+campo+'&txt='+txt;
		retorno = new Ajax.Request(url, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta});	 
	}
	
	function carregando(){
		$('loader').style.visibility='visible';	
		$('modal').style.visibility='visible';
	}
	function carregado(){
		$('loader').style.visibility='hidden';	
		$('modal').style.visibility='hidden';
	}
	function falha(){
	  alert('falha no carregamento');
	  carregado();
	}
	function resposta(resp){
	    carregado();
		var json = resp.responseText;
		if (json == 'ok'){
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/atendentes.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		}else{
		  alert('Ocorreu erro que impossibilitou a altera��o, por favor, tente novamente!');
		}			
	}
	function cadastra(){
	  nome     = $('nome').value;
	  user     = $('user').value;  	
	  checagem  = true;
	  
	  if (nome == ''){
		alert('Informe corretamente o nome do atendente');
		$('nome').focus();
		checagem = false;
	  }
	  else{
	    if (user == ''){
		  	alert('Informe corretamente o User Name');
			$('user').focus();
			checagem = false;
		}	   
	  }
	  if(checagem){
		var url2 = 'cadastrandoAtendente.php?nm='+nome+'&nameuser='+user+'&idDepartamento='+idDepartamento+'&idUser='+idUserLogado;
		retorno2 = new Ajax.Request(url2, {method: 'get',onLoading:carregando, onFailure: falha, onSuccess: resposta2});
	  }
	}
	
	function resposta2(resp2){
	    var json2 = resp2.responseText;
		carregado();

		if (json2 == 'ok'){ 
		 alert('A senha deste atendente �: "123456"');
		  var jsonStore2 = new dojo.data.ItemFileWriteStore({ url: "json/atendentes.php" , clearOnClose: true}); 
		  grid.setStore(jsonStore2);
		  document.usuarios.reset();
		}else{
		  alert('Devido falha, n�o foi efetivado o cadastro do atendente!');
		}  
			  
	}
</script>
<?php

}//fachando o teste se est� logado!!
?>
<style type="text/css">		
	    @import "../dijit/themes/tundra/tundra.css";		
		@import "../dojox/grid/resources/Grid.css";		
		@import "../dojox/grid/resources/tundraGrid.css";		
	</style>
</head>

<body class="tundra">
 <div id="loader"><img src="img/loading.gif" />Carregando</div>
 <div id="modal"></div>
 <h1>Gest�o de Usu�rios</h1>
 <?php
     //checando se tem alguem logado, �ltima tentativa de seguran�a. se n�o tiver logado, daqui n�o passa
 if ($_SESSION['iduser'] > 0){
    ?>
				   <div id="form" class="dialog">
					  <form name="formulario" method="post" dojoType="dijit.form.Form">
						 <label>Nome &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input dojoType="dijit.form.TextBox" name="nome" id="nome" />
						 <label>User Name</label><input dojoType="dijit.form.TextBox" name="user" id="user"  />
						 <br />
						 <label>Departamento</label><div id="selectDepartamento"></div>
					  </form>
					  <div id="btns2">
							<button dojoType="dijit.form.Button" onClick="cadastra()" id="new">Novo</button>
							<button dojoType="dijit.form.Button" onclick="document.usuarios.reset();" id="limpa">Limpar</button>
					  </div>
				   </div> <!-- fecha o formul�rio -->			   
				   <div id="gridNode" jsId="grid1"></div>
  <?php
 }else{
 	echo "Usu�rio n�o est� logado!";
 }	 
   ?>		
</body>
</html>
