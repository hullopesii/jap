<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Jata&iacute; Auto Pe&ccedil;as - A empresa</title>
<link href="supercss.css" rel="stylesheet" type="text/css" />
<?php 
	require_once("classes/Layout.php");
	
	
	$template = new Layout();
	
	echo $template->tag('A empresa');
	
?>

</head>

<body>
  <div id="mask"></div>
  <div class="ceu"></div>
  <div class="faixaPneuFundo"></div>
  <!-- page is beginning here -->
  <div class="grid">
      <div class="fone"><img src="img/phone.png" alt="telefone" title="Loja 1" class="img" /><span>Loja 1 - 64. 3631-2676</span>
	   <img src="img/phone.png" alt="telefone" title="Loja 2" class="img" /><span>Loja 2 - 64. 3631-2058</span>
	   </div>
	   <a href="/" title="clique para voltar � home"><div class="logo"></div></a>
	   
	   <div class="topo"><img src="img/fotoTopo1.png" /></div>
	   
	   <div class="biela"></div>
	   
	   <div class="faixaPneuMenu">
			<?php //menu
			   echo $template->menu('A empresa');
			?>
	   </div>
	   <div class="content">
	     <!-- beginning the content's site -->
		   <div id="colEsq">
		    <h2>Loja 1</h2>
		    <img src="img/fotosPorta.jpg" alt="fotos da jata&iacute; auto pe&ccedil;as loja 1" />
			<h2>Loja 2</h2>
			<img src="img/fotosPorta2.jpg" alt="fotos da jata&iacute; auto pe&ccedil;as loja 2" />
			 
			 <h2>Pe�as automotivas para as marcas</h2>
			 <img src="img/marcasPecas.jpg" alt="marcas que atendemos: audi, chevrolet, citroen, fiat, ford, gm, nissan, mitsubishi, peugeot, renaut, toyota, volare, volkswagen" />	 
			    
			 
		   </div>
		   <div id="colDir"> 		
		      <h2>Saiba mais sobre nossa empresa</h2>  
			      
		      <p>A JATA� AUTO PE�AS, empresa genuinamente Jataiense, atua no varejo de pe�as automotivas para toda a cidade e regi�o.</p>
			  <p>A mais de 26 anos temos atuado sempre focados no bom atendimento, na honestidade e na qualidade dos produtos que fornecemos</p>
			  <p>Nosso intuito � o fornecimento da pe�a ideal para a sua necessidade. Para isso, s� trabalhamos com as melhores marcas, tanto para pe�as originais, quanto para pe�as paralelas, praticando o melhor pre�o de toda a regi�o.</p>
			  <h2>Nossas Lojas</h2>
			  <p>Rec�m inaugurada, nossa segunda loja visa ampliar nossa for�a de atendimento, estando mais pr�ximo e melhor atendendo a todos nossos clientes. Devido o grande crescimento de nossa cidade, foi de extrema import�ncia que pass�ssemos a atuar com duas lojas bem montadas.</p>
			  <h2>Nossos Clientes</h2>
			  <p>O prop�sito de a Jata� Auto Pe�as existir � atender as necessidades dos clientes. Isto est� incluso um bom atendimento, pre�os justos, prazos, praticidade em encontrar o que precisa de pe�as mec�nicas para o seu carro e rapidez na entrega. Atendemos todos os nossos clientes de linha leve a utilit�rios como F4000, F350, etc.</p>
			  


<h2>Inaugura��o Filial Jata� Auto Pe�as</h2>

<p>A Jata� Auto Pe�as � uma empresa que nasceu na cidade de Jata� � Goi�s. A 27 anos somos referencia na cidade no segmento de varejo em autope�as e buscando sempre uma vis�o de crescimento para atender de maneira mais r�pida e pratica os nossos clientes. Para isso foi inaugurada dia 21/07/2012 uma nova filial situada na avenida Said de Abdalla n�244 em frente ao Curinga dos Pneus. </p><p>
Nessa nova filial possu�mos uma oficina mec�nica parceira para executar a parte de servi�os de repara��o para autom�veis e utilit�rios tanto nacionais como importados.</p>

			  <p>Lembre-se, estamos sempre � disposi��o para quaisquer d�vidas ou outros esclarecimentos. Entre em contato, teremos o prazer em serv�-lo</p>
			  
			  <p>Atrav�s de nosso novo site, partimos para o atendimento de todo o Brasil, levando toda a nossa excel�ncia em vendas de pe�as automotivas ao pa�s inteiro.</p>
			  <h2>Nossa cidade</h2>
			  <p><a href="acidade.php">Clique aqui para conhecer um pouco mais nossa cidade.</a></p>
			  <hr />
			  <h2>Nossas Lojas</h2>
			  <li>Loja 1</li>
			  <p><img src="img/btnHome2.png" width="30px" height="30px" alt="endere�o" />Rua Benjamim Constant, n� 1163<br />centro - Jata�-Goi�s CEP: 75800-016</p>
			  <p><img src="img/phone.png" alt="telefone" />64. 3631-2676</p>
			  <hr />
			  <li>Loja 2</li>
			  <p><img src="img/btnHome2.png" width="30px" height="30px" alt="endere�o" />Avenida Said Abdala , n�244<br />
		     Bairro Jardim Rio Claro- Jata�-Goi�s CEP: 75802-019 </p>
			  <p>Caixa postal 310 </p>
			  <p><img src="img/phone.png" alt="telefone" />64. 3631-2058</p>

			  <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=jata%C3%AD+auto+pe%C3%A7as&amp;aq=&amp;sll=-14.408749,-54.042208&amp;sspn=48.392441,79.013672&amp;ie=UTF8&amp;hq=auto+pe%C3%A7as&amp;hnear=Jata%C3%AD+-+Goi%C3%A1s&amp;ll=-17.900589,-51.712884&amp;spn=0.006013,0.009645&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=8611688956563952072&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=jata%C3%AD+auto+pe%C3%A7as&amp;aq=&amp;sll=-14.408749,-54.042208&amp;sspn=48.392441,79.013672&amp;ie=UTF8&amp;hq=auto+pe%C3%A7as&amp;hnear=Jata%C3%AD+-+Goi%C3%A1s&amp;ll=-17.900589,-51.712884&amp;spn=0.006013,0.009645&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=8611688956563952072" style="color:#0000FF;text-align:left">Exibir mapa ampliado</a></small>
	       </div> 
		   <div class="empurra"></div>
		 <!-- //content-->
	   </div>
	   <?php //rodape
	      echo $template->rodape();
	   ?>
  </div>
  <!-- page is over here -->
</body>
</html>