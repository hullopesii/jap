<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link href="supercss2.css" rel="stylesheet" type="text/css" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8"> 
	<title>Visualiza&ccedil;&atilde;o de ve&iacute;culo</title> 
	 
	<link rel="stylesheet" href="galleriffic/css/galleriffic-3.css" type="text/css" /> 
	<script type="text/javascript" src="galleriffic/js/jquery-1.3.2.js"></script> 
	<script type="text/javascript" src="galleriffic/js/jquery.history.js"></script> 
	<script type="text/javascript" src="galleriffic/js/jquery.galleriffic.js"></script> 
	<script type="text/javascript" src="galleriffic/js/jquery.opacityrollover.js"></script> 
		
	<link href="supercss2.css" rel="stylesheet" type="text/css" />	
</head> 
<body>
<?php
  $id =  $_GET['id'];
  
  require_once('classes/Connection.php');
  
  $query = new Connection();
  
  //contador de acessos
  $str="INSERT INTO `visualizacao` (`idVisualizacao` , `idProduto`) VALUES (NULL ,  '".$id."');";
  $query->getConsulta2($str);
  
  //contando acessos
  $conta = $query->getConsulta2("SELECT * FROM `visualizacao` WHERE `idProduto`=".$id);
  $qtdVisualizacao = $conta->num_rows;
  //---//contador
  
  $resp = $query->getConsulta2("SELECT * FROM  `produto` INNER JOIN categoria ON categoria.idCategoria = produto.idcategoria WHERE `idProduto`=$id");
  
  $row = $resp->fetch_assoc();
  
  echo "<h1>".$row['nmProduto']."</h1>";
  
?>
 <div class="visualizacao">Visualizações: <?php echo $qtdVisualizacao; ?></div>
 <div class="detalhes">
   <h4>Detalhes do produto</h4>
   <img src="img/faixaLi.png" />
   <p>Categoria: <?php echo $row['categoria']; ?></p>
   <?php if ($row['mostraValor'] == 'Sim'){echo "<p>Valor R$ ". $row['valor']."</p>";}else{echo "<p>Valor R$ Sob-consulta</p>";} ?>
   <?php	    
	    echo $row['descricao1'];
	?>
</div>
  <div class="itens">
    <h4>Mais informações</h4>
    <img src="img/faixaLi.png" /><br />
	<?php	    
	    echo $row['descricao2'];
	?>
  </div>
  <div class="titleFotos"><h4>Fotos do produto</h4><img src="img/faixaLi.png" /></div>
				<!-- Start Advanced Gallery Html Containers --> 
  <div id="gallery" class="content"> 
	<div id="controls" class="controls"></div> 
	<div class="slideshow-container"> 
	  <div id="loading" class="loader"></div> 
	  <div id="slideshow" class="slideshow"></div> 
	</div> 
  </div>
				<div id="thumbs" class="navigation"> 
					<ul class="thumbs noscript"> 
					<?php
					  $idproduto = $row['idProduto'];
					  $count = 0;	
					  $resp2 = $query->getConsulta2("SELECT * FROM `fotosprodutos` WHERE (`idProduto` = $idproduto)");
					  while ($row2= $resp2->fetch_assoc()){
					    $count++;//contando as fotos, filho
						//echo "<img src='fotos/".$idproduto."/thumb".$row2['foto']."' class='thumb' />";
						echo "<li>";
						echo "<a class='thumb' name='leaf' href='produtos/".$idproduto."/".$row2['foto']."' title='".$row2['nome']."'>"; 
						echo "	<img src='produtos/".$idproduto."/thumb".$row2['foto']."' alt='".$row2['nome']."' class='miniT' />"; 
						echo "</a>";
						echo "</li>"; 
					  }
					?>
				  </ul> 
				</div> 
				<!-- End Advanced Gallery Html Containers --> 
				<div style="clear: both;"></div> 
       <a href="solicita.php?id=<?php echo $idproduto; ?>" title="clique para solicitar mais informações sobre este produto"><div class="btnMais"></div></a>

		<script type="text/javascript"> 
			jQuery(document).ready(function($) {
                // We only want these styles applied when javascript is enabled
                $('div.navigation').css({'width' : '300px', 'float' : 'left'});
                $('div.content').css('display', 'block');

                // Initially set opacity on thumbs and add
                // additional styling for hover effect on thumbs
                var onMouseOutOpacity = 0.67;
                $('#thumbs ul.thumbs li').opacityrollover({
                    mouseOutOpacity:   onMouseOutOpacity,
                    mouseOverOpacity:  1.0,
                    fadeSpeed:         'fast',
                    exemptionSelector: '.selected'
                });
                
                // Initialize Advanced Galleriffic Gallery
                var gallery = $('#thumbs').galleriffic({
                    delay:                     3500,
                    numThumbs:                 6,
                    preloadAhead:              10,
                    enableTopPager:            true,
                    enableBottomPager:         true,
                    maxPagesToShow:            7,
                    imageContainerSel:         '#slideshow',
                    controlsContainerSel:      '#controls',
                    captionContainerSel:       '#caption',
                    loadingContainerSel:       '#loading',
                    renderSSControls:          true,
                    renderNavControls:         true,
                    playLinkText:              'Play Slideshow',
                    pauseLinkText:             'Pause Slideshow',
                    prevLinkText:              '&lsaquo; Foto Anterior',
                    nextLinkText:              'Próxima Foto &rsaquo;',
                    nextPageLinkText:          'Próxima &rsaquo;',
                    prevPageLinkText:          '&lsaquo; Anterior',
                    enableHistory:             false,
                    autoStart:                 <?php if ($count > 1) echo 'true'; else echo 'false';?>,
                    syncTransitions:           true,
                    defaultTransitionDuration: 900,
                    onSlideChange:             function(prevIndex, nextIndex) {
                        // 'this' refers to the gallery, which is an extension of $('#thumbs')
                        this.find('ul.thumbs').children()
                            .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                            .eq(nextIndex).fadeTo('fast', 1.0);
                    },
                    onPageTransitionOut:       function(callback) {
                        this.fadeTo('fast', 0.0, callback);
                    },
                    onPageTransitionIn:        function() {
                        this.fadeTo('fast', 1.0);
                    }
                });
            });
		</script> 
</body> 
</html>