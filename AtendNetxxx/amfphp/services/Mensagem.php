<?php
class Mensagem{
		public $con = null;
		
		public function __construct(){
		   require_once("classes/Connection.php");
		   $query = new Connection();
		   $this->con = $query;
		}
		
		public function hora(){
		  return date ("Y-m-d H:i:s");
		}
		
		public function getIds($idAtendimento, $qual){
		  $st = "SELECT * FROM `atendimento` WHERE idAtendimento=$idAtendimento";
		  $resp = $this->con->getConsulta2($st);
		  
		  $row = $resp->fetch_assoc();
		  if ($qual == 'atendente'){
		  	//$resp2 = $this->con->getConsulta2('SELECT * FROM `atendente` WHERE idAtendente='.$row['idAtendente']);
			//$row2 = $resp2->fetch_assoc();
			return $row['idAtendente'];
		  }else{
		    //$resp2 = $this->con->getConsulta2('SELECT * FROM `usuario` WHERE idUsuario='.$row['idUsuario']);
			return $row['idUsuario'];
		  }	
		  
		}
		
		public function statusUsuario($id, $status){
		  //$idUsuario = $this->getIds($id, 'usuario');
		  $st = "UPDATE `usuario` SET `status`='$status' WHERE `idUsuario` = $id";
		  
		  $resp = $this->con->getConsulta2($st);
		}
		public function statusAtendente($id, $status){
		  //$idAtendente = $this->getIds($id, 'atendente');
		  $st = "UPDATE `atendente` SET `status`='$status' WHERE `idAtendente` = $idUsuario";
		  
		  $resp = $this->con->getConsulta2($st);
		}
		
		public function abreAtendimento($idUser, $idAt){
		  $resp = $this->con->getConsulta2("INSERT INTO `atendimento` VALUES(null, $idUser, $idAt, 'Aberto')");
		  $id = $this->con->lastId;
		  $this->statusAtendente($idAt, 'Ocupado');
		  $this->statusUsuario($idUser, 'Atendimento');
		  
		  return $id;
		  
		}
		public function entraNoAtendimento($idAtendimento){
		  $idAtendente = $this->getIds($idAtendimento, 'atendente');
		  //$this->statusAtendente($idAtendente, 'Ocupado');
		}
		public function fechaAtendimento($idAtendimento){
            $resp = $this->con->getConsulta2("UPDATE `atendimento` SET `status` = 'Concluido' WHERE `idAtendimento`=$idAtendimento");		  
 		}
		
		public function firstMensagem($idAtendimento){
		   $time = $this->hora();
		   //$this->statusUsuario($idAtendimento, 'Atendimento');//mudando o status do usu�rio
		   //$this->statusAtendente($idAtendimento, 'Ocupado');
		   
		   $st = "INSERT INTO `mensagem` VALUES (null, '$time', 'Ol�, em que posso ajudar?', 'Atendente', $idAtendimento)";
		   
		   $resp = $this->con->getConsulta2($st);
		   if ($resp > 0){
		     return 'ok';
		   }else{
		     return 'erro';
		   }
		}
		
		public function lastMensagem($idUser, $idAtendimento){

		  $this->statusUsuario($idUser, 'Concluido');
		  $this->fechaAtendimento($idAtendimento);
		  
		  $this->setMensagem($idAtendimento, 'Agradecemos o contato, estamos � disposi��o!', 'Atendente');
		  
		}
		public function sairAtendimento($idAtendimento, $ho){
		   $idUser = $this->getIds($idAtendimento, 'usuario');
		   $this->statusUsuario($idUser, 'Concluido');
		   $this->fechaAtendimento($idAtendimento);
		   
		   $this->setMensagem($idAtendimento, "$ho saiu do atendimento", $ho);
		}
		
		
		public function atendente($id){
		   $st = "SELECT * FROM `atendente` WHERE `idAtendente`=$id";
		   $resp = $this->con->getConsulta2($st);
		   $row = $resp->fetch_assoc();
		   return $row['nmAtendente'];
		}
		
		public function getMensagem($id){
		   //primenramente, checando se o atendimento ainda est� aberto
		   $st0 = "SELECT * FROM `atendimento` WHERE `idAtendimento`=$id";
		   $resp0 = $this->con->getConsulta2($st0);
		   $row0 = $resp0->fetch_assoc();
		   if ($row0['status'] !== 'Concluido'){
				   $st ="SELECT * FROM mensagem inner join atendimento on atendimento.idAtendimento = mensagem.idAtendimento WHERE mensagem.idAtendimento =$id"; 
						
					$resp = $this->con->getConsulta2($st);
					$i = 0;
					 while ($row = $resp->fetch_array()) {
							$nm = '';
							if ($row['quem'] == 'Usuario'){
							   $nm = $this->pegaNm($row['idUsuario'], 'usuario');
							}else{
							   $nm = $this->pegaNm($row['idAtendente'], 'atendente');
							}
							$arrai[] = array('idMensagem' => $row['idMensagem'], 'time' => $this->date_time($row['time'], "H:i:s", false), 'mensagem' => $row['mensagem'], 'nome' => $nm);
							$i++;
					   }
				   return $arrai;
			}else{
			   return 'Fechado';
			}	   
		}
		
		public function pegaNm($id, $ho){
		   $st = "SELECT * FROM `$ho` WHERE ";
		   if ($ho == 'atendente'){$st .= "idAtendente = $id";}
		   if ($ho == 'usuario'){$st .= "idUsuario = $id";}
		   $resp = $this->con->getConsulta2($st);
		   $row = $resp->fetch_array();
		   return $row[1];
		}
		
		public function setMensagem($idAtendimento, $msg, $ho){
		   $time = $this->hora();
		   $st = "INSERT INTO `mensagem` VALUES (null, '$time', '$msg', '$ho', '$idAtendimento')";
		   
		   $resp = $this->con->getConsulta2($st);
		   if ($resp > 0){
		     return 'ok';
		   }else{
		     return 'erro';
		   }
		}
		
		
		
		public function date_time($date_time, $output_string, $utilizar_funcao_date = false) {
			// Verifica se a string est� num formato v�lido de data ("aaaa-mm-dd" ou "aaaa-mm-dd hh:mm:ss")
			if (preg_match("/^(\d{4}(-\d{2}){2})( \d{2}(:\d{2}){2})?$/", $date_time)) {
					$valor['d'] = substr($date_time, 8, 2);
					$valor['m'] = substr($date_time, 5, 2);
					$valor['Y'] = substr($date_time, 0, 4);
					$valor['y'] = substr($date_time, 2, 2);
					$valor['H'] = substr($date_time, 11, 2);
					$valor['i'] = substr($date_time, 14, 2);
					$valor['s'] = substr($date_time, 17, 2);
			
			// Verifica se a string est� num formato v�lido de hor�rio ("hh:mm:ss")
			} else if (preg_match("/^(\d{2}(:\d{2}){2})?$/", $date_time)) {
					$valor['d'] = NULL;
					$valor['m'] = NULL;
					$valor['Y'] = NULL;
					$valor['y'] = NULL;
					$valor['H'] = substr($date_time, 0, 2);
					$valor['i'] = substr($date_time, 3, 2);
					$valor['s'] = substr($date_time, 6, 2);
			
			} else {
			return false;
		    }

			if ($utilizar_funcao_date) {
			return date($output_string, mktime($valor['H'], $valor['i'], $valor['s'], $valor['m'], $valor['d'], $valor['Y']));
			}
			
			foreach (array('d', 'm', 'Y', 'y', 'H', 'i', 's') as $caractere) {
				$output_string = ereg_replace("(^|[^\\\\])".$caractere, "\\1".$valor[$caractere], $output_string);
			}
			
			$output_string = ereg_replace("(^|[^\\\\])\\\\", "\\1", $output_string);
			
			return $output_string;
			
}


}