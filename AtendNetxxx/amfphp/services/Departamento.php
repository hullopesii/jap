<?php
class Departamento{
		public $con = null;
		
		public function __construct(){
		   require_once("classes/Connection.php");
		   $query = new Connection();
		   $this->con = $query;
		}
		
		public function getDepartamentos(){
		   //return "Voc� digitou: $nm , $mail";
		   $st ="SELECT * 
				FROM  `departamento`
				INNER JOIN atendente ON atendente.idDepartamento = departamento.idDepartamento
				WHERE atendente.status <>  'Ausente'"; 
				
		    $resp = $this->con->getConsulta2($st);
		    $i = 0;
		     while ($row = $resp->fetch_object()) {
                    $ArrayOfDepartamentos[] = $row;
					$i++;
               }
			 if ($i == 0){
			   $ArrayOfDepartamentos['departamento'] = 'Nenhum Atendente';
			   $ArrayOfDepartamentos['idDepartamento'] = 0; 
			 }  
            return($ArrayOfDepartamentos);
		}
}
