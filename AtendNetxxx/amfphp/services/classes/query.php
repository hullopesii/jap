<?php
	class Query {
	    
		private $stg;
		private $con;
		
		public function __construct() {
			$this->stg = '';
		}
		public function conectaBD(){
			//$conect = new mysqli("localhost", "hullopes", "tatuapu", "tbshotelcombr_admin");//local
			$this->con = new mysqli("localhost", "hullopes", "tatuapu", "admin_geo");//local			
			
		}

		public function desconectBD(){
			$this->con->close();
		}
		
		public function setQuery($s){
			$this->stg = $s;
		}
		
		public function getQuery(){
		
			$this->conectaBD();										//conecta
			
			$arrayResp["resposta"]= $this->con->query($this->stg);	//consulta
			
			if (!$arrayResp["resposta"]) {  //teste de erro de mysql
    			$arrayResp["erro"] = $this->con->error;
			}else{
			   $arrayResp["erro"]='';
			 }  
			$this->desconectBD();									//fecha conex�o
			
			return $arrayResp;
		}
		
	}
	/*
	$query = new Query;
	
	$query->setQuery('select * from `produto`');
	
	$result = $query->getQuery();
	print_r($result);
    */
?>