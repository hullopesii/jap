<?php
   session_start("cotacao");
   
   require_once("../classes/Connection.php");
   $query = new Connection();
   
   require_once("../classes/Funcoes.php");
   $funcoes = new Funcoes();
   
   $data = '';
   $nm = '';
   if ($_GET){
      $for = $_SESSION['idFornecedor'] = $_GET['for'];
	  $id  = $_SESSION['idCotacao'] = $_GET['id'];
	  //echo "SELECT * FROM `cotacoes` WHERE (`idCotacao`={$id} and `status`= 'Ativo')";
	  $resp = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE (`idCotacao`={$id} and `status`= 'Ativa')");
	  
	  if ($resp->num_rows > 0){
	     //cota��o correta
		 $row = $resp->fetch_assoc();
		 $data = 'Cota��o n� '. $row['idCotacao'];
		 $data .=' de '. $funcoes->trataData($row['data']);
	  }else{
	     header("location:http://www.jataiautopecas.com.br");
	  }
	  
	  //checando se o fornecedor est� correto
	  $resp1 = $query->getConsulta2("SELECT * FROM `fornecedores` WHERE (`idFornecedor`={$for})");
	  
	  if ($resp1->num_rows > 0){
			 //cota��o correta
			 //vendo se o fornecedor j� n�o enviou seus pre�os para esta cota��o
			 $respR = $query->getConsulta2("SELECT * FROM `respitemcotacao` WHERE (`idCotacao`={$id} and `idFornecedor`={$for}) ");
			 if ($respR->num_rows > 0){
			      header("location:jarespondel.php");
			 }else{		 
					 $row1 = $resp1->fetch_assoc();
					 $nm = $_SESSION['fornecedornm'] = $row1['fornecedor'];
			 }		 
	  }else{
	     header("location:http://www.jataiautopecas.com.br");
	  }
	  
   }
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cota&ccedil;&atilde;o de pe&ccedil;as/produtos Jata&iacute; Auto Pe&ccedil;as <?php echo $data; ?></title>

<script type="text/javascript" src="../scripts/jquery.1.5.2.min.js"></script>
<script type="text/javascript" src="../scripts/jquery.ui.js"></script>
<script type="text/javascript" src="../scripts/jquery.validate.js"></script>
<script>

  $().ready(function() {
		$("#formulario").validate();
	});
	
   function submita(){
      $("#formulario").submit();
   }	
	
</script>
<style>
  .error{ color:#CC0000;}
</style>

</head>

<body style="background-color:#CCCCCC">
 <center>
   <div style="width:940px; padding:10px; text-align:justify; background-color:#FFFFFF;">
	<img src="../img/logo.png" />
	
	   <h2><?php if($nm !== '') echo "Seja Bem vindo, {$nm}"; ?></h2>
	   <p>Segue abaixo a lista dos produtos em cota��o. Favor preencher os campos e clicar no bot�o Salvar.<br />Caso n�o tenha o produto cotado, favor deixar em branco.</p>
	 <form method="post" action="salvaCotacao.php" id="formulario">  
	   <table width="100%" border="0" cellspacing="5" cellpadding="3">
		  <tr bgcolor="#CCCCCC">
			<td width="13%">C�digo</td>
			<td width="42%">Descri��o</td>
			<td width="9%">Qtd cotada</td>
			<td width="9%">Marca</td>
			<td width="16%">Valor Unit�rio</td>
		  </tr>
		  <?php
		      //listando os itens
			  $resp2 = $query->getConsulta2("SELECT * FROM `itenscotacoes` WHERE `idCotacao`={$id}");
			  
			  while ($row2 = $resp2->fetch_assoc()){
			      echo "
				     <tr>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['codigo']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['descricao']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px; text-align:right'>{$row2['qtdCotada']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['referencia']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'><input type='text' class='number' name='{$row2['idItem']}' /></td>
					  </tr>
				  ";
			    
			  }
		  ?>
		</table>
		<input type="button" value="Salvar" style=" width:200px; height:40px; float:right;" onclick="submita()" />
		<div style="clear:both"></div>
       </form>
	
   </div>
 </center>  	
</body>
</html>
