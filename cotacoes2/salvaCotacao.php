<?php
   session_start("cotacao");
  
   if (isset($_SESSION['idFornecedor'])){
      if ($_SESSION['idFornecedor'] == 0) header("location:http://www.jataiautopecas.com.br");
   }	
	
	
	$for = $_SESSION['idFornecedor'];
	$id  = $_SESSION['idCotacao'];
   
   require_once("../classes/Connection.php");
   $query = new Connection();
   
   require_once("../classes/Funcoes.php");
   $funcoes = new Funcoes();
   
   $data = '';
   $nm = '';

      
	  
	  $resp = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE (`idCotacao` in ({$id}) and `status`= 'Ativa')");
	  
	  if ($resp->num_rows > 0){
	     //cota��o correta
		 $row = $resp->fetch_assoc();
		 $data = 'Cotação nº '. $row['idCotacao'];
		 $data .=' de '. $funcoes->trataData($row['data']);
	  }else{
	     header("location:http://www.jataiautopecas.com.br");
	  }
	  
	  //checando se o fornecedor est� correto
	  $resp1 = $query->getConsulta2("SELECT * FROM `fornecedores` WHERE (`idFornecedor`={$for})");
	  
	  if ($resp1->num_rows > 0){
	     //cota��o correta
		 $row1 = $resp1->fetch_assoc();
		 $nm = $_SESSION['fornecedornm'] = $row1['fornecedor'];
	  }else{
	     header("location:http://www.jataiautopecas.com.br");
	  }
	  
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cota&ccedil;&atilde;o de pe&ccedil;as/produtos Jata&iacute; Auto Pe&ccedil;as <?php echo $data; ?></title>

<script type="text/javascript" src="../scripts/jquery.1.5.2.min.js"></script>
<script type="text/javascript" src="../scripts/jquery.ui.js"></script>
<script type="text/javascript" src="../scripts/jquery.validate.js"></script>
<script>

  $().ready(function() {
		$("#formulario").validate();
	});
	
	 function submita(){
      $("#formulario").submit();
   }
   function goBack(){
      location.href='index.php?id=<?php echo $id."&for=".$for; ?>';
   }
</script>
<style>
  .error{ color:#CC0000;}
</style>

</head>

<body style="background-color:#CCCCCC">
 <center>
   <div style="width:940px; padding:10px; text-align:justify; background-color:#FFFFFF;">
	<img src="../img/logo.png" />
	
	   <h2><?php if($nm !== '') echo "{$nm}, "; ?></h2>
	   <p>Favor conferir os valores informados abaixo!</p>
	   <p>Estando corretos, clique em "Salvar e Enviar".</p>
	 <form method="post" action="salvaCotacao2.php" id="formulario">  
	   <table width="100%" border="0" cellspacing="5" cellpadding="3">
		  <tr bgcolor="#CCCCCC">
			<td width="13%">Código</td>
			<td width="42%">Descrição</td>
			<td width="9%">Qtd cotada</td>
			<td width="9%">Unidade</td>
			<td width="16%">Valor R$</td>
		  </tr>
		  <?php
		      //listando os itens
			 // $resp2 = $query->getConsulta2("SELECT * FROM `respitemcotacaotemp` INNER JOIN `itenscotacoes` on `itenscotacoes`.`idItem` = `respitemcotacaotemp`.`idItemCotacao` WHERE ((`respitemcotacaotemp`.`idCotacao` in ({$id})) and (`respitemcotacaotemp`.`idFornecedor`={$for}))");
			  $resp2 = $query->getConsulta2("SELECT * , SUM( qtdCotada ) AS  'total' FROM  `respitemcotacaotemp` INNER JOIN itenscotacoes on itenscotacoes.idItem = respitemcotacaotemp.idItemCotacao WHERE ((respitemcotacaotemp.idCotacao in ({$id})) and (respitemcotacaotemp.idFornecedor = {$for})) GROUP by itenscotacoes.codigo");
			  
			  while ($row2 = $resp2->fetch_assoc()){
			      $ii = $row2['idItem'];
				  $valor = number_format($row2['preco'], 2, ",", ".");
				  $name = $funcoes->getIdsItensConcatenados($id, $row2['codigo']);//recebe os idItens dos itens concatenados das cota��es
				  echo "
				     <tr>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['codigo']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['descricao']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px; text-align:right'>{$row2['total']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['referencia']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'><input type='text' class='number' name='{$name}' value='{$valor}' /></td>
					  </tr>
				  ";
			  }
			  
			  //fechando a sess�o
			  //$_SESSION['idFornecedor'] = 0;
			  
		  ?>
		</table>
		<input type="button" value="Ver Lista Completa" style=" width:250px; height:40px; float:right;" onclick="goBack()" />
		<input type="button" value="Salvar e Enviar" style=" width:200px; height:40px; float:right;" onclick="submita()" />
		<div style="clear:both"></div>
       </form>
       </form>
	
   </div>
 </center>  	
</body>
</html>
