<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Jata&iacute; Auto Pe&ccedil;as - Cat&aacute;logo de Produtos - Pe&ccedil;as para carros - pe&ccedil;as automotivas- Jata&iacute;, Mineiros e Rio Verde</title>
<link href="supercss.css" rel="stylesheet" type="text/css" />
    <meta name="author" content="Pandorati" /> 
    <meta name="copyright" content="jata� auto pe�as - Todos os direitos reservados" />
	<meta name="description" content="Pe�as automotivas para jata�, mineiros, rio verde, goi�s e regi�o. Listagem de produtos" />
    <meta name="robots" content="index, follow" /> 
    <meta name="revisit-after" content="7 days" /> 
<?php 
	require_once("classes/Layout.php");
	require_once("classes/Listagem.php");
	
	$template = new Layout();
	
	$list = new Listagem();
	
	echo $template->tag('Produtos');
	
?>
<script src="script/jquery-1.4.2.js" type="text/javascript" charset="utf-8"></script> 

<script type="text/javascript" src="scripts/home.js"></script>

</head>

<body>
  <div id="mask"></div>
  <div class="ceu"></div>
  <div class="faixaPneuFundo"></div>
  <!-- page is beginning here -->
  <div class="grid">
       <div class="fone"><img src="img/phone.png" alt="telefone" title="Loja 1" class="img" /><span>Loja 1 - 64. 3631-2676</span>
	   		<img src="img/phone.png" alt="telefone" title="Loja 2" class="img" /><span>Loja 2 - 64. 3631-2058</span>
	   </div>
	   <div class="topo"><img src="img/fotoTopo1.png" /></div>
	   
	   <div class="biela"></div>
	   
	   <div class="faixaPneuMenu">
			<?php //menu
			   echo $template->menu('produtos');
			?>
	   </div>
	   <div class="content2">
	     <!-- beginning the content's site -->
		   <div id="colEsq">
			 
			 <h2>Escolha pela Marca:</h2>
			 <ul>
			 <?php
			    $respMarcas = $list->listaMarcas();
			    while ($rowMarcas = $respMarcas->fetch_assoc()){
				  echo "<li class='liMarcas'><a href='?idMarca=".$rowMarcas['idMarca']."'>".$rowMarcas['Marca']."</a></li>";
				}
			 ?>    
			 </ul>
			 <br />
			 <h2>Escolha pela Categoria de produto:</h2>
			 <ul>
			 <?php
			    $respCategoria = $list->listaCategorias();
			    while ($rowCategorias = $respCategoria->fetch_assoc()){
				  echo "<li class='liMarcas'><a href='?idCategoria=".$rowCategorias['idCategoria']."'>".$rowCategorias['categoria']."</a></li>";
				}
			 ?>    
			 </ul>
		   </div>
		   <div id="colDir"> 		
		      <h2>Produtos</h2>  
			   <div class="portaProdutos2">
					 <?php
					 $idMarca = 0;
					 $idCategoria =0;
					 if (!empty($_GET['idMarca'])){
					   $idMarca = $_GET['idMarca'];
					 }
					 if (!empty($_GET['idCategoria'])){
					   $idCategoria = $_GET['idCategoria'];
					 }
					 
					  $TAMANHO_PAGINA = 12;
					  if (empty($_GET['i'])){
						$inicio = 0; 
						$pagina=1;
					  }else{
						$pagina = $_GET['i'];
						$inicio = ($pagina - 1) * $TAMANHO_PAGINA;
					  } 
		
					   $num_total_registros = $list->conta();
					   
						//calculo o total de p�ginas 
					   $total_paginas = ceil($num_total_registros / $TAMANHO_PAGINA); 
						
						
					  $res = $list->lista3($inicio, $idCategoria, $idMarca);
				    if (mysqli_num_rows($res) == 0){
					  echo "<p>Nenhum resultado encontrado para a sua busca</p>";
					}	
				while($row = $res->fetch_assoc()){
					$idproduto = $row['idProduto'];
					$conect = $list->construct();	
					$resp2 = $conect->getConsulta2("SELECT * FROM `fotosprodutos` WHERE (`idProduto` = $idproduto)");
					
					$cont = 0;
					$fotobyid='';
					while($row2 = $resp2->fetch_assoc()){
						if ($cont < 1){
							$src = $row2['foto'];
							$fotobyid[] = $row2['nome'];
						}else{
							$fotobyid[] = $row2['nome'];
						}
					}
					$fjs = "";
					for($x=0;$x<count($fotobyid);$x++){
						$fjs .="-".$fotobyid[$x];
					}
					$fotos[$idproduto] = $fotobyid;
					echo "<div class='carro' onClick='abreModal(".$idproduto.");'>";
					
					echo "<div class='foto'><img src='produtos/".$idproduto."/thumb".$src."' /></div>";
					echo " <div class='dados'><h2>".$row['nmProduto']."</h2>";
					echo "<p>Categoria: ".$row['categoria']."</p>";
					if ($row['mostraValor'] == 'Sim'){echo "<p>Valor R$ ". $row['valor']."</p>";}else{echo "<p>Valor R$ Sob-consulta</p>";}
					echo "</div>";
					echo "</div>";
				}
			  		?>
			  </div>
			  
			  <div class="empurra"></div> 
			  <div class="passador">
				  <?php //passador de p�ginas
					   //mostro os diferentes �ndices das p�ginas, se � que h� v�rias p�ginas 
						if ($total_paginas > 1){
						   echo "P�ginas : "; 
						   for ($i=1;$i<=$total_paginas;$i++){ 
							  if ($pagina == $i) 
								 //se mostro o �ndice da p�gina actual, n�o coloco link 
								 echo $pagina; 
							  else 
								 //se o �ndice n�o corresponde com a p�gina mostrada actualmente, coloco o link para ir a essa p�gina 
								 echo "<a href='?i=" . $i . "' class='linkPassa'>" . $i . "</a> "; 
						   }
						} 
				  ?>
				  </div>   
		      			  
	       </div> 
		 <!-- //content-->
	   </div>
	   <?php //rodape
	      echo $template->rodape();
	   ?>
  </div>
  <!-- page is over here -->
   <!-- #dialog � o id do DIV definido como mostrado a seguir  -->
	 <div id="boxes">
	   <div id="dialog2" class="window">
	    <!-- Bot�o para fechar a janela tem class="close" -->
	    <a href="#" class="close"></a>
		<iframe frameborder="0" width="100%" height="510px" id="iframe"></iframe>
	   </div>
	  </div>
</body>
</html>