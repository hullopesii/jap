<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Jata&iacute; Auto Pe&ccedil;as - Entre em contato, pe&ccedil;as automotivas para todas as marcas</title>
<link href="supercss.css" rel="stylesheet" type="text/css" />
<?php 
	require_once("classes/Layout.php");
	
	
	$template = new Layout();
	
	echo $template->tag('Contato');
	
?>
<script type="text/javascript" src="scripts/jquery-1.4.2.js"></script>  
<script src="script/jquery.cycle.all.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="scripts/jquery.tooltip.js"></script>-->
<script type="text/javascript" src="scripts/home2.js"></script>
<script type="text/javascript" src="script/jquery.ui.js"></script>

</head>

<body>
  <div id="mask"></div>
  <div class="ceu"></div>
  <div class="faixaPneuFundo"></div>
  <!-- page is beginning here -->
  <div class="grid">
       <div class="fone"><img src="img/phone.png" alt="telefone" class="img" /><span>64. 3631-2676</span></div>
	   <a href="/" title="clique para voltar � home"><div class="logo"></div></a>
	   
	   <div class="topo"><img src="img/fotoTopo1.png" /></div>
	   
	   <div class="biela"></div>
	   
	   <div class="faixaPneuMenu">
			<?php //menu
			   echo $template->menu('contato');
			?>
	   </div>
	   <div class="content">
	     <!-- beginning the content's site -->
		   <div id="colEsq">
		    <h2>Nossa Localiza��o no mapa</h2>
			 	    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?ie=UTF8&amp;q=jata%C3%AD+auto+pe%C3%A7as&amp;fb=1&amp;gl=br&amp;hq=auto+pe%C3%A7as&amp;hnear=Jata%C3%AD+-+GO&amp;hl=pt-BR&amp;view=map&amp;f=d&amp;daddr=Rua+Benjamin+Constant+Q,+37+-+Jata%C3%AD+-+GO,+75800-016&amp;geocode=CRRLvyK0ARsTFVwe7_4d1NPq_CHIESa0et2Cdw&amp;ved=0CBcQ_wY&amp;ei=DV9gTOvZAqX8yAW-stGdBQ&amp;ll=-17.883556,-51.719212&amp;spn=0.006295,0.006615&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com.br/maps?ie=UTF8&amp;q=jata%C3%AD+auto+pe%C3%A7as&amp;fb=1&amp;gl=br&amp;hq=auto+pe%C3%A7as&amp;hnear=Jata%C3%AD+-+GO&amp;hl=pt-BR&amp;view=map&amp;f=d&amp;daddr=Rua+Benjamin+Constant+Q,+37+-+Jata%C3%AD+-+GO,+75800-016&amp;geocode=CRRLvyK0ARsTFVwe7_4d1NPq_CHIESa0et2Cdw&amp;ved=0CBcQ_wY&amp;ei=DV9gTOvZAqX8yAW-stGdBQ&amp;ll=-17.883556,-51.719212&amp;spn=0.006295,0.006615&amp;source=embed" style="color:#0000FF;text-align:left">Exibir mapa ampliado</a></small>
			 
		   </div>
		   <div id="colDir"> 		
		      <h2>Entre em contato conosco</h2>  
			  <p align="center"><img src="img/btnHome2.png" width="30px" height="30px" alt="endere�o" />Rua Benjamim Constant, n� 1163<br />centro - Jata�-Goi�s CEP: 75800-016</p>
			  <p align="center"><img src="img/phone.png" alt="telefone" />64. 3631-2676</p>
			  <div id="formContato">
				  <div id="carrega2"><img src="img/ajax-loader.gif" /></div> 
					   <form method="post" action="#" name="contact" id="contact">
					   <div id="resposta"></div>
					   <table width="100%" border="0" cellspacing="3" cellpadding="0">
						  <tr>
							<td class="td1">Seu Nome:</td>
							<td class="td2"><input name="nome" id="nome" type="text" /></td>
						  </tr>
						  <tr>
							<td class="td1">Seu Email:</td>
							<td class="td2"><input name="email" id="email" type="text" /></td>
						  </tr>
						  <tr>
							<td class="td1">Seu Telefone:</td>
							<td class="td2"><input name="fone" id="fone" type="text" /></td>
						  </tr>
						</table>
						 <center>
						   Mensagem:
						   <textarea name="mensagem" id="mensagem" cols="40" rows="4"></textarea>
						   <img src="img/btnOk.png" onclick="enviaContato();" alt="Enviar" title="Clique para enviar" class="imgBtn" />
						 </center>  
						</form>
			   </div>
				 
              <br />
	       </div> 
		 <!-- //content-->
	   </div>
	   <?php //rodape
	      echo $template->rodape();
	   ?>
  </div>
  <!-- page is over here -->
  <!-- #dialog � o id do DIV definido como mostrado a seguir  -->
	 <div id="boxes">
	   <div id="dialog2" class="window">
	    <!-- Bot�o para fechar a janela tem class="close" -->
	    <a href="#" class="close"></a>
		<iframe frameborder="0" width="100%" height="510px" id="iframe"></iframe>
	   </div>
	  </div>
</body>
</html>