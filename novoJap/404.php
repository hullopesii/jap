<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="Auto peças Jataí - Erro 404 not found">
<meta name="robots" content="noodp">
<link rel="canonical" href="http://jataiautopecas.com.br/">
<meta property="og:locale" content="pt_BR">
<meta property="og:type" content="website">
<meta property="og:title" content="Auto peças Jataí - Erro 404 not found">
<meta property="og:description" content="Auto peças Jataí - Erro 404 not found">
<meta property="og:url" content="http://jataiautopecas.com.br/">
<meta property="og:site_name" content="Jataí Auto Peças">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="Auto peças Jataí - Erro 404 not found">
<meta name="twitter:title" content="Auto peças Jataí - Erro 404 not found">


<link rel="stylesheet" id="vegas-css" href="/novoJap/files/jquery.vegas.css" type="text/css" media="all">
<link rel="stylesheet" id="wp-pagenavi-css" href="/novoJap/files/pagenavi-css.css" type="text/css" media="all">

<link rel="stylesheet" id="A2A_SHARE_SAVE-css" href="/novoJap/files/addtoany.min.css" type="text/css" media="all">
<style id="A2A_SHARE_SAVE-inline-css" type="text/css">
@media screen and (min-width:981px){
.a2a_floating_style.a2a_default_style{display:none;}
}
</style>
<script type="text/javascript" src="/novoJap/files/jquery.js"></script>
<script type="text/javascript" src="/novoJap/files/jquery-migrate.min.js"></script>
<script type="text/javascript" src="/novoJap/files/addtoany.min.js"></script>

<link rel="shortlink" href="http://jataiautopecas.com.br/">

<script type="text/javascript">
var a2a_config=a2a_config||{};a2a_config.callbacks=a2a_config.callbacks||[];a2a_config.templates=a2a_config.templates||{};a2a_localize = {
	Share: "Compartilhar",
	Save: "Salvar",
	Subscribe: "Inscrever",
	Email: "Email",
	Bookmark: "Favoritos",
	ShowAll: "Mostrar tudo",
	ShowLess: "Mostrar menos",
	FindServices: "Procurar serviço(s)",
	FindAnyServiceToAddTo: "Encontrar rapidamente qualquer serviço para",
	PoweredBy: "Serviço fornecido por",
	ShareViaEmail: "Share via email",
	SubscribeViaEmail: "Subscribe via email",
	BookmarkInYourBrowser: "Adicionar aos favoritos",
	BookmarkInstructions: "Press Ctrl+D or \u2318+D to bookmark this page",
	AddToYourFavorites: "Adicionar a favoritos",
	SendFromWebOrProgram: "Send from any email address or email program",
	EmailProgram: "Email program",
	More: "Mais"
};

a2a_config.color_main = "92639";a2a_config.color_border = "AECADB";a2a_config.color_link_text = "333333";a2a_config.color_link_text_hover = "333333";
</script>
<script type="text/javascript" src="/novoJap/files/page.js" async="async"></script>

			
<link rel="icon" href="/novoJap/img/icone.png" sizes="32x32">


<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="author" content="Tatuapu">

<title>Jataí Auto Peças - Error 404. Recurso não disponível!</title>

<link href="/novoJap/files/css" rel="stylesheet" type="text/css">
<link href="/novoJap/files/css(1)" rel="stylesheet" type="text/css">
<link href="/novoJap/files/css(2)" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/novoJap/css/font-awesome.min.css">
<link rel="shortcut icon" href="/novoJap/img/icone.png" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="/novoJap/files/reset.css">
<link rel="stylesheet" type="text/css" href="/novoJap/files/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/novoJap/css/style.css">
<link rel="stylesheet" type="text/css" href="/novoJap/files/owl.carousel.css">

<script src="/novoJap/files/jquery.min.js"></script>
<script src="/novoJap/files/TweenMax.min.js"></script>
<script src="/novoJap/files/effects.js"></script><!-- efeitos básicos de menu e página -->


</head>


<body>
<!-- fb sdk JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9&appId=277798568961484";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- close fb sdk JavaScript -->

<img class="vegas-background" src="/novoJap/img/autoparts.jpg" style="position: fixed; left: 0px; top: -147.833px; width: 1366px; height: 910.667px; bottom: auto; right: auto;"><div class="vegas-overlay" style="margin: 0px; padding: 0px; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; background-image: url(&quot;no&quot;);"></div>

<script>
    jQuery( function() {
    jQuery.vegas('slideshow', {
    delay:5000,
    backgrounds:[
    	{ src:'/novoJap/img/imgFundo1.jpg', fade:1000},
    	{ src:'/novoJap/img/imgFundo2.jpeg', fade:1000},
    	{src:'/novoJap/img/img/autoparts.jpg', fade:1000}
    	] 
})     ('overlay', {src:'no'}) }); 
</script>


<div class="page-wrap">    
	<div id="topo"> 
    	<div id="logo"></div><!--logo--> 
        <div id="btn_menu_mob">
            <i class="fa fa-bars fa-2x"></i>
        </div><!--btn_menu_mobile-->
        
        <div id="menu_mob" style="display: none; z-index: 9;">
            <div class="menu-menu-principal-container">
            	<ul id="menu-menu-principal" class="menu">
            		<li id="menu-item-21" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-21">
            			<a href="/novoJap/" title="Clique para voltar à página principal">Home</a>
            		</li>
					<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
						<a href="/novoJap/sobre-nos/" title="Saiba mais sobre a Jataí Auto Peças">Sobre Nós</a>
					</li>
					<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
						<a href="/novoJap/nossos-produtos" title="Produtos Jataí Auto Peças">Nossos Produtos</a>
					</li>
					<li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22">
						<a href="/novoJap/contato" title="Fale com a Jataí Auto Peças">Contato</a>
					</li>
				</ul>
			</div>
		</div><!--menu_mobile-->
    </div><!--topo-->
    
    <div class="menu_lateral">
        <div id="menu_dsktp">
        	<div class="telefones" style="text-align: center;">
            	<h3>Fones</h3>
                <h3>(64) 3605-3000</h3>
                <h3>(64) 3631-2058</h3>
            </div>
            <div class="menu-menu-principal-container">
            	<ul id="menu-menu-principal-1" class="menu">
            		<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-21"><div class="menu_marker menu_marker_active" style="display: none;"></div><a href="/novoJap/">Home</a></li>		
								
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><div class="menu_marker"></div><a href="/novoJap/sobre-nos/">Sobre Nós</a></li>
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25"><div class="menu_marker"></div><a href="/novoJap/nossos-produtos/">Nossos Produtos</a></li>
					<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22"><div class="menu_marker"></div><a href="/novoJap/contato/">Contato</a></li>
				</ul>
			</div>
		</div><!--menu_dsktp-->
        
        <div class="social_media">
        <i class="fa fa-facebook fa-2x" onclick="window.open('https://www.facebook.com/jataiautopecas','_blank');"></i>
        <i class="fa fa-youtube fa-2x" onclick="window.open('https://www.youtube.com/channel/UC7XcQHVXv3SeyDylQ9cPeRA','_blank');"></i>   
        <i class="fa fa-map fa-2x" onclick="window.open('https://goo.gl/maps/McxXfCBMMNN2','_blank');"></i>
        <i class="fa fa-newspaper-o fa-2x" onclick="window.open('/novoJap/news','_self');"></i>            
        </div><!--redes_sociais-->
    </div><!--sidebar_menu-->
    
    <div id="box_content">
 		
        <div class="row_site">
        	
                <div class="col-esq-content base_content">
                	<h1>Ops....</h1>
                    <br/>
                    <p>O recurso que você está procurando não existe, ou foi movido. 
					<a href="/novoJap/">Volte à nossa página Principal</a>
					Ou use nosso menu para encontrar o que está procurando.
					</p>             
                </div>
                <div class="col-dir-content img-base">
                	<img src="/novoJap/img/caixa.png"/>
                </div><!--col-esq-content-->
           </div><!--row_site-->
           
        <div class="row_site">	
	        	
        </div><!--row_site-->
                
    </div><!--box_content-->
   



<div class="a2a_kit a2a_kit_size_32 a2a_floating_style a2a_default_style" style="bottom: 0px; left: 0px; line-height: 32px;">
	<a class="a2a_dd addtoany_share_save" href="https://www.addtoany.com/share#url=http://www.jataiautopecas.com.br%2F&amp;title=Jataí Auto Peças">
	<span class="a2a_svg a2a_s__default a2a_s_a2a" style="background-color: rgb(1, 102, 255);">
		<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
			<g fill="#FFF">
				<path d="M14 7h4v18h-4z"></path>
				<path d="M7 14h18v4H7z"></path>
			</g>
		</svg>
	</span>
	<span class="a2a_label">Compartilhar</span>
	</a>
	<div style="clear: both;"></div>
</div>			

		
<script type="text/javascript" src="/novoJap/files/jquery.vegas.js"></script>


</div><!--page-wrap-->

<footer class="site-footer">

	<div class="rights">
		<p>
			<a href="/novoJap/">Jataí Auto Peças</a> - 2017 - 
			<a href="/novoJap/dicas-automotivas-jap">Dicas Automotivas JAP</a>
		</p>
	</div>
	
</footer><!--site-footer--> 

<script src="/novoJap/files/bootstrap.min.js"></script>
<script src="/novoJap/owl.carousel.min.js"></script>
<script src="/novoJap/hotspot-map.min.js"></script>

<!-- End Google Tag Manager -->


</body>
</html>