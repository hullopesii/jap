<?php
namespace View;

use Model\Post, DAO\PostDAO;


class PostView
{
	/**
	* parâmetro de entrada é um array multi
	* sendo que em cada linha há um array com o nome da categoria
	* a quantidade esperada, o título para a seção
	* e o link para mostrar mais
	* ex getPosts(array(array("produtos-facebook",1,"Produtos","nossos-produtos"))
	**/
	public static function getPosts($arr){
		$html = "";
		$boxOpen  = "
		<div id='box_content'>
		";
		$boxClose = "
		</div>
		";
		$c=0;

		foreach ($arr as $chave => $valor) {
			
			$postDAO = new PostDAO();
			$posts = $postDAO->procura($valor[0],$valor[1]);
			$html.="
			<h3 style='padding-left:60px;'>{$valor[2]}</h3>
			";
			foreach ($posts as $key => $value) {
				if ($c%2==0)	
					$html.= '
					<div class="col-esq-content base_content">
					';
				else
					$html.= '
					<div class="col-dir-content base_content">
					';
				$html.= $value['post_content'];		
				$html.= "
					</div>
					";
				$c++;
			}
			if($valor[3]!='')
			$html.="
			<h4 style='padding-left:60px;padding-top:-60px;'><a href='/novoJap/{$valor[3]}'>Ver mais {$valor[2]} [+]</a></h4>
			";		
		}
		if ($c>0)
			$html = $boxOpen . $html . $boxClose;
		else
			$html = "";
		return $html;
	}
	
}