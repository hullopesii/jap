<?php
namespace util;

interface DAO
{
	public function atualizar($obj);
    
    public function excluir($obj);
    
    public function listaTodos();
    
    public function procura($obj,$tpBusca);
    
    public function salvar($obj);
}