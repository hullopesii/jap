<?php
namespace Model;
class Post
{
	private $post_date;
	private $post_content;
	private $post_title;
	private $post_name;
	private $post_type;
	private $post_status;

	public function __construct($post_date = null,$post_content = null, $post_title=null, $post_name=null, $post_type="post",$post_status=null){
		$this->post_date = $post_date;
		$this->post_content = $post_content;
		$this->post_title = $post_title;
		$this->post_name = $post_name;
		$this->post_type = "post";
		$this->post_status = $post_status;
	}
	public function getPostDate(){
		return $this->post_date;
	}
	public function getPostContent(){
		return $this->post_content;
	}
	public function getPostTitle(){
		return $this->post_title;
	}
	public function getPostName(){
		return $this->post_name;
	}
	public function getPostType(){
		return $this->post_type;
	}
	public function getPostStatus(){
		return $this->post_status;
	}
}