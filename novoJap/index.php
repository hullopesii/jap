<?php
require_once("util/Url.php");

    /**
    * pegando o primeiro parâmetro passado após a primeira barra. 
    * o parâmetro 0 indica o primeiro parâmetro esperado
    * 1 para o segundo,  e assim por diante
    **/
    $modulo = Url::getURL(0);
    $parametro1 = Url::getURL(1);
    $parametro2 = Url::getURL(2);

    if( $modulo == null )
        $pagina = "indexb.php";
    else
    	$pagina = "indexc.php";    

    if( file_exists($pagina))
        require($pagina);
    else
        require("404.php");    
