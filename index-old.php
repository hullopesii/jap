<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Jata&iacute; Auto Pe&ccedil;as - Pe&ccedil;as para todas as marcas de ve&iacute;culos</title>
<link href="supercss.css" rel="stylesheet" type="text/css" />
	<meta name="author" content="Pandorati" /> 
    <meta name="copyright" content="jata� auto pe�as - Todos os direitos reservados" />
	<meta name="description" content="Pe�as automotivas para jata�, mineiros, rio verde, goi�s e regi�o." />
    <meta name="robots" content="index, follow" /> 
    <meta name="revisit-after" content="7 days" /> 
<?php 
	require_once("classes/Layout.php");
	
	require_once("classes/Listagem.php");
	//require_once("AtendNet/amfphp/services/classes/ChecaAtendimento.php");
	//$depto = new ChecaAtendimento();
	//echo $depto->checa();
	$template = new Layout();
	
	$list = new Listagem();
	echo $template->tag('home');
	
?>
<script src="script/jquery-1.4.2.js" type="text/javascript" charset="utf-8"></script> 
<script src="script/jquery.cycle.all.min.js" type="text/javascript"></script> 
<script src="script/banners.js" type="text/javascript"></script> 
<script type="text/javascript" src="scripts/home.js"></script>

</head>

<body>
  <div id="mask"></div>
  <div class="ceu"></div>
  <div class="faixaPneuFundo"></div>
  <!-- page is beginning here -->
  <div class="grid">
       <div class="fone"><img src="img/phone.png" alt="telefone" title="Loja 1" class="img" /><span>Loja 1 - 64. 3631-2676</span>
	   <img src="img/phone.png" alt="telefone" title="Loja 2" class="img" /><span>Loja 2 - 64. 3631-2058</span>
	   </div>
	   <!--<div class="atendimento"><a href="http://www.jataiautopecas.com.br/AtendNet/AtendNet.html" title="Atendimento On-line" target="_blank"><img src="img/btnAtendimentoOnline.png" border="0" /></a></div>-->
	   <a href="/" title="clique para voltar � home"><div class="logo"></div></a>
	   
	   <div class="topo"><img src="img/fotoTopo1.png" /></div>
	   
	   <div class="biela"></div>
	   
	   <div class="faixaPneuMenu">
			<?php //menu
			   echo $template->menu('home');
			?>
	   </div>
	   <div class="content">
	     <!-- beginning the content's site -->
		   <div id="colEsq">
		     <h2>Bem vindo ao nosso site</h2>	
			 <div class="fachadaHome">
			   <div class="fotoFachada"><img src="img/fotoFachada.jpg" alt="foto da fachada jap" /></div>
			   <div class="txtFachada">Jata� Auto Pe�as � uma empresa especializada na comercializa��o de pe�as automotivas para as mais variadas marcas.<br />
			   Agora com duas lojas para melhor atender aos clientes de Jata&iacute; e toda a regi&atilde;o. <br />
			   <a href="aempresa.php"  title="Saiba mais sobre n�s" class="linkNormal">Saiba Mais</a> 
             </div>
			 </div>
			 <br />
			 <h2>Nossas lojas</h2>
			 <hr />
			 <p>
				<li>Loja-1</li>
				Av Benjamin Constant 1163 Centro<br />
				<img src="img/phone.png" alt="telefone" class="img" width="15" height="15" />64. 3631-2676<br />
			</p>	
			<hr />
			<li>Loja-2</li>
			Av Said Abdalla 244 Jardin Rio Claro<br />
			<img src="img/phone.png" alt="telefone" class="img" width="15" height="15" />64. 3631-2058
			
			 <h2 class="spaco">Pe�as automotivas para as marcas</h2>
			 <img src="img/marcasPecas.jpg" alt="marcas que atendemos: audi, chevrolet, citroen, fiat, ford, gm, nissan, mitsubishi, peugeot, renaut, toyota, volare, volkswagen" />	 
			    
			 <h2 class="spaco">Principais Marcas que trabalhamos</h2>
			 	  <div id="mostrador1"> 
					<a href="#"> 
						<img src="img/marcas1.jpg" border="none"/> 
					</a> 
					<a href="consultoria.php"> 
						<img src="img/marcas2.jpg" border="none"/> 
					</a> 
					<a href="#"> 
						<img src="img/marcas3.jpg" border="none"/> 
					</a> 
					<a href="#"> 
						<img src="img/marcas4.jpg" border="none"/> 
					</a> 
			      </div><!-- // mostrador1 --> 
				  <div id="portaPager"><div id="pager"></div></div>
		   </div>
		   
		   <div id="colDir"> 
		      
			  <h2>Produtos em destaque</h2>
		      <div class="portaProdutos">
					 <?php
					
					  $TAMANHO_PAGINA = 8;
					  if (empty($_GET['i'])){
						$inicio = 0; 
						$pagina=1;
					  }else{
						$pagina = $_GET['i'];
						$inicio = ($pagina - 1) * $TAMANHO_PAGINA;
					  } 
		
					   $num_total_registros = $list->conta();
					   
						//calculo o total de p�ginas 
					   $total_paginas = ceil($num_total_registros / $TAMANHO_PAGINA); 
						
						
					  $res = $list->lista2($inicio);
					
				while($row = $res->fetch_assoc()){
					$idproduto = $row['idProduto'];
					$conect = $list->construct();	
					$resp2 = $conect->getConsulta2("SELECT * FROM `fotosprodutos` WHERE (`idProduto` = $idproduto)");
					
					$cont = 0;
					$fotobyid='';
					while($row2 = $resp2->fetch_assoc()){
						if ($cont < 1){
							$src = $row2['foto'];
							$fotobyid[] = $row2['nome'];
						}else{
							$fotobyid[] = $row2['nome'];
						}
					}
					$fjs = "";
					for($x=0;$x<count($fotobyid);$x++){
						$fjs .="-".$fotobyid[$x];
					}
					$fotos[$idproduto] = $fotobyid;
					echo "<div class='carro' onClick='abreModal(".$idproduto.");'>";
					
					echo "<div class='foto'><img src='produtos/".$idproduto."/thumb".$src."' /></div>";
					echo " <div class='dados'><h2>".$row['nmProduto']."</h2>";
					echo "<p>Categoria: ".$row['categoria']."</p>";
					if ($row['mostraValor'] == 'Sim'){echo "<p>Valor R$ ". $row['valor']."</p>";}else{echo "<p>Valor R$ Sob-consulta</p>";}
					echo "</div>";
					echo "</div>";
				}
			  		?>
			  </div>
			  <div class="passador">
				  <?php //passador de p�ginas
					   //mostro os diferentes �ndices das p�ginas, se � que h� v�rias p�ginas 
						if ($total_paginas > 1){
						   echo "P�ginas : "; 
						   for ($i=1;$i<=$total_paginas;$i++){ 
							  if ($pagina == $i) 
								 //se mostro o �ndice da p�gina actual, n�o coloco link 
								 echo $pagina; 
							  else 
								 //se o �ndice n�o corresponde com a p�gina mostrada actualmente, coloco o link para ir a essa p�gina 
								 echo "<a href='?i=" . $i . "' class='linkPassa'>" . $i . "</a> "; 
						   }
						} 
				  ?>
		     </div>
	       </div> 
		   <div class="empurra"></div>
		 <!-- //content-->
	   </div>
	   <?php //rodape
	      echo $template->rodape();
	   ?>
  </div>
  <!-- page is over here -->
  <!-- #dialog � o id do DIV definido como mostrado a seguir  -->
	 <div id="boxes">
	   <div id="dialog2" class="window">
	    <!-- Bot�o para fechar a janela tem class="close" -->
	    <a href="#" class="close"></a>
		<iframe frameborder="0" width="100%" height="510px" id="iframe"></iframe>
	   </div>
	  </div>
</body>
</html>