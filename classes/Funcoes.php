<?php //classe de fun�oes diversas
	class Funcoes {
		public $conec;
	
		public function __construct(){
			require_once("Connection.php");
			$this->conec = new Connection();
		}
		
		function pagina(){
		
			$stringServer= $_SERVER['REQUEST_URI'];
	
		$arrayExplode = explode("/",$stringServer);
		$ar2 = explode(".", $arrayExplode[1]);
		return $subPagina =  $ar2[0];
		}
		
		public function constataItemInserido($for, $id, $idItem){
			 $resp = $this->conec->getConsulta2("SELECT * FROM `respitemcotacaotemp` WHERE ((`idItemCotacao` in ({$idItem})) and (
			`idFornecedor` = {$for}) and (`idCotacao` in ({$id})))");			
			return $resp->num_rows;
				
		}
		public function constataItemInserido2($for, $id, $idItem){
			 $resp = $this->conec->getConsulta2("SELECT * FROM `respitemcotacaotemp` WHERE ((`idItemCotacao` in ({$idItem})) and (
			`idFornecedor` = {$for}) and (`idCotacao` in ({$id})))");			
			return $resp->num_rows;
				
		}
		public function constataItemInseridoFim($for, $id, $idItem){
			 $resp = $this->conec->getConsulta2("SELECT * FROM `respitemcotacao` WHERE ((`idItemCotacao` in ({$idItem})) and (
			`idFornecedor` = {$for}) and (`idCotacao` in ({$id})))");			
			return $resp->num_rows;
				
		}
		
		public function insereItem($for, $id, $k, $valor){
		    //inserindo o item da cota��o definitivamente
			//separando os id, que � o id dos fornecedores
			$expIds = explode(',',$id);
			if (count($expIds>0)){
				foreach($expIds as $i){//varrendo o array explodido, para pegar cada id do fornecedor, caso esteja concatenado
					//primeiro, vamos checar se existe este item $k para esta cota��o
					$resp0 = $this->conec->getConsulta2("SELECT * FROM `itenscotacoes` WHERE (idCotacao = {$i} and idItem = {$k})");
					if ($resp0->num_rows > 0){ //opa, ent�o existe este item nesta cota��o
						$str = "INSERT INTO `respitemcotacao` VALUES (NULL, ".$k.", {$for}, {$i}, {$valor})";
						$this->conec->getConsulta2($str);
					}	
				}
			}else{
				//opa, n�o foi concatenado
				    //primeiro, vamos checar se existe este item $k para esta cota��o
					$resp0 = $this->conec->getConsulta2("SELECT * FROM `itenscotacoes` WHERE (idCotacao = {$i} and idItem = {$k})");
					if ($resp0->num_rows > 0){ //opa, ent�o existe este item nesta cota��o
						$str = "INSERT INTO `respitemcotacao` VALUES (NULL, ".$k.", {$for}, {$i}, {$valor})";
						$this->conec->getConsulta2($str);
					}	
			}	
			
		}
		
		public function insereItemTemp($for, $id, $k, $valor){
			//separando os id, que � o id dos fornecedores
			$expIds = explode(',',$id);
			if (count($expIds>0)){
				foreach($expIds as $i){//varrendo o array explodido, para pegar cada id do fornecedor, caso esteja concatenado
					//primeiro, vamos checar se existe este item $k para esta cota��o
					$resp0 = $this->conec->getConsulta2("SELECT * FROM `itenscotacoes` WHERE (idCotacao = {$i} and idItem = {$k})");
					if ($resp0->num_rows > 0){ //opa, ent�o existe este item nesta cota��o
						$str = "INSERT INTO `respitemcotacaotemp` VALUES (NULL, ".$k.", {$for}, {$i}, {$valor})";
						$this->conec->getConsulta2($str);
					}	
				}
			}else{
				//opa, n�o foi concatenado
				    //primeiro, vamos checar se existe este item $k para esta cota��o
					$resp0 = $this->conec->getConsulta2("SELECT * FROM `itenscotacoes` WHERE (idCotacao = {$i} and idItem = {$k})");
					if ($resp0->num_rows > 0){ //opa, ent�o existe este item nesta cota��o
						$str = "INSERT INTO `respitemcotacaotemp` VALUES (NULL, ".$k.", {$for}, {$i}, {$valor})";
						$this->conec->getConsulta2($str);
					}	
			}	
			
		}
		public function alteraItemTemp($valor, $k, $for, $id){
			$str = "UPDATE  `respitemcotacaotemp` SET  `preco` =  '{$valor}' WHERE  ((`idItemCotacao`={$k}) and (`idFornecedor` = {$for}) and (`idCotacao` in  ({$id})))";
			$this->conec->getConsulta2($str);
		}
		
		public function alteraItem($valor, $k, $for, $id){
			$str = "UPDATE  `respitemcotacao` SET  `preco` =  '{$valor}' WHERE  ((`idItemCotacao`={$k}) and (`idFornecedor` = {$for}) and (`idCotacao` in  ({$id})))";
			$this->conec->getConsulta2($str);
		}
		
		public function getIdsItensConcatenados($ids, $cod){
			 $resp = $this->conec->getConsulta2("SELECT * FROM `itenscotacoes` WHERE (idCotacao in({$ids}) and codigo={$cod})");
			 $cods = '';
			 while ($row = $resp->fetch_assoc()){
			 	$cods.=$row['idItem'].',';
			 }			
			 $cods = substr($cods,0,-1);
			return $cods;
				
		}
		
		public function checkValue($for, $id, $idItem){
			$resp = $this->conec->getConsulta2("SELECT * FROM `respitemcotacaotemp` WHERE ((`idItemCotacao`={$idItem}) and (
			`idFornecedor` = {$for}) and (`idCotacao` = {$id}))");
			
			$valor ='0.0';
			if ($resp->num_rows>0){
				$row = $resp->fetch_assoc();
				$valor = $row['preco'];
			}
			if ($valor !== '0.0')
				return number_format($valor,2,',','.');
			else
				return '';	
		}
		
		public function checkValue2($for, $id, $idItem){
			$resp = $this->conec->getConsulta2("SELECT * FROM `respitemcotacaotemp` WHERE ((`idItemCotacao`={$idItem}) and (
			`idFornecedor` = {$for}) and (`idCotacao` in ({$id})))");
			//echo "SELECT * FROM `respitemcotacaotemp` WHERE ((`idItemCotacao`={$idItem}) and (`idFornecedor` = {$for}) and (`idCotacao` in ({$id})))";
			
			$valor ='0.0';
			if ($resp->num_rows>0){
				$row = $resp->fetch_assoc();
				$valor = $row['preco'];
			}
			if ($valor !== '0.0')
				return number_format($valor,2,',','.');
			else
				return '';	
		}
		
		function trataData($obj){
			//tratando a data
			 $data = explode("/", $obj);
			 if (sizeof($data) > 1){
			   $dataCorrigida = $data[2].'/'.$data[1].'/'.$data[0];
			 }else{
			   $data = explode("-", $obj);
			   $dataCorrigida = $data[2].'/'.$data[1].'/'.$data[0];
			 }
			 return $dataCorrigida;
  		}
	}
?>