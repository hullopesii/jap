<?php
	class Layout {
		
		function rodape(){
		  $html = '';
		  $html .= '
		  <div class="rodape">jataiautopecas.com.br | 2012 - &reg; - <a href="http://www.pandorati.com.br" target="_blank" class="linkRodape" title="Sites Inteligentes">feito por pandorati</a></div>';		  
		  return $html;
		}
		
		
		function checaIp(){
				$ip = $_SERVER["REMOTE_ADDR"];
				$suspeitos = array('189.56.75.3','189.108.191.3', '200.178.254.162', '200.203.146.76','187.6.209.250', '187.58.193.91','189.5.178.12');
				if (in_array($ip, $suspeitos)){
					header('location:suspect.php');
				}
				require_once("Connection.php");
				$con = new Connection();
				$str = "INSERT INTO `acessos` Values (null, '{$ip}', '".date("Y/m/d")."')";
				$con->getConsulta2($str);
		}
		
		function tag($pg){
		  $this->checaIp();
		  require_once('Connection.php');
		  
		  $con = new Connection();
		  
		  $str = "SELECT * FROM `keywords` WHERE (`pagina`= '$pg' and `tipo`= 'meta')";
		  
		  $resp = $con->getConsulta2($str);
		  $cont = mysqli_num_rows($resp);
		  $i=1;
		  $html = "<link rel='shortcut icon' href='img/icone.png' type='image/x-icon' />";
		  $html .= " <script type=\"text/javascript\">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37813432-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> ";
		  $html.= "<meta name='keywords' content='";
		  while($row= $resp->fetch_assoc()){
		  		$html.=$row['palavra'];
				if ($i < $cont){
				 $html.=', ';
				}			
				$i++;	
		  }
		  
		  return $html . "'/>";
		}
		
		function menu($pg){
		 $classHome = 'btnMenuOff';
		 $classAempresa = 'btnMenuOff';
		 $classContato = 'btnMenuOff';
		 $classProdutos = 'btnMenuOff';
		 switch($pg){
		   case "home":
		      $classHome = 'btnMenuOn';
			  break;
		   case "A empresa":
		      $classAempresa = 'btnMenuOn';
			  break;
		   case "contato":
		      $classContato = 'btnMenuOn';
			  break;
		   case "produtos":
		      $classProdutos = 'btnMenuOn';
			  break;	  	  	  
		 }
		 $html = '
		    <div class="menu">
				 <a class="linkBtn" href="/"><div class="'.$classHome.'">Home</div></a>
				 <a class="linkBtn" href="aempresa.php" title="Saiba Mais Sobre N�s"><div class="'.$classAempresa.'">A Empresa</div></a>
				 <a class="linkBtn" href="contato.php" title="Entre em contato"><div class="'.$classContato.'">Contato</div></a>
				 <a class="linkBtn" href="produtos.php" title="Rela��o de Produtos"><div class="'.$classProdutos.'">Produtos</div></a>
			</div>
		    ';
		 return $html;
		}		
		
		function nocache(){
			
				header('Expires: Fri, 25 Dec 1980 00:00:00 GMT');
				header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');
				header('Cache-Control: no-cache, must-revalidate');
				header('Pragma: no-cache');

		}
		function getBrowser()
		{
			$var = $_SERVER['HTTP_USER_AGENT'];
			$info['browser'] = "OTHER";
			
			// valid brosers array
			$browser = array ("MSIE", "OPERA", "FIREFOX", "MOZILLA",
							  "NETSCAPE", "SAFARI", "LYNX", "KONQUEROR");
	
			// bots = ignore
			$bots = array('GOOGLEBOT', 'MSNBOT', 'SLURP');
	
			foreach ($bots as $bot)
			{
				// if bot, returns OTHER
				if (strpos(strtoupper($var), $bot) !== FALSE)
				{
					return $info;
				}
			}
			
			// loop the valid browsers
			foreach ($browser as $parent)
			{
				$s = strpos(strtoupper($var), $parent);
				$f = $s + strlen($parent);
				$version = substr($var, $f, 5);
				$version = preg_replace('/[^0-9,.]/','',$version);
				if (strpos(strtoupper($var), $parent) !== FALSE)
				{
					$info['browser'] = $parent;
					$info['version'] = $version;
					return $info;
				}
			}
			return $info;
		}//fecha browser7
		
		
		
		
	}
?>