<?php //classe de fun�oes de listagens de intens do banco de dados
	class Listagem {
		private $conecta;
		public $arContaMenores = array();
		public $arrPrecos = array();
		
		
		function construct() {
			require_once("Connection.php");
			return $conecta = new Connection();
		}	
		
		public function idMim($item, $valor, $maior){
		     $conecta = $this->construct();
			 
			 //temos o id do item cotado e seu menor valor, fazendo a busca pelos produtos com este c�digo e pre�o
			 $r = $conecta->getConsulta2("SELECT * FROM  `respitemcotacao` WHERE ( idItemCotacao ={$item} AND preco = {$valor})");
			 $ar = array(); 
			 $ar['menor'] = $valor;
			 $ar['maior'] = $maior;
 			 
			 while ($row = $r->fetch_assoc()){				   
							
							$idFornecedor = $row['idFornecedor'];							
							$ar['id'] = $row['idRespCotacao'];	//guardando os ids das cota��es vencedoras	
							//$ar['fornecedor'] = $idFornecedor;
							$this->arrPrecos[$idFornecedor][] = $ar;			 				
			 }			 
		}
		
		function contaGanhos2($idC){
		    $this->arrPrecos = ""; //zerando o array
		    $conecta = $this->construct();
			
			//pegando as respostas das cota��es
			 $resp1 = $conecta->getConsulta2("SELECT * FROM `respitemcotacao` WHERE idCotacao={$idC}");
			 //echo "SELECT * FROM `respitemcotacao` WHERE idCotacao={$idC}";
			 
			 $ar = array();
			 //$arIds = array();
			 
			 while ($row1=$resp1->fetch_assoc()){			 	   
				   $ar = "";			       
				   $idItem = $row1['idItemCotacao'];
				   //pegando os menores e maiores valores para o item
			       $r = $conecta->getConsulta2("SELECT *, min(preco) as 'menor', max(preco) as 'maior' FROM `respitemcotacao` WHERE (`idItemCotacao`={$idItem})");
				   //echo "SELECT *, min(preco) as 'menor', max(preco) as 'maior' FROM `respitemcotacao` WHERE (`idItemCotacao`={$idItem})";
				   $rw = $r->fetch_assoc();
				   //pegando os ids das respostas ganhadoras por item
				   $this->idMim($idItem, $rw['menor'], $rw['maior']);				   
			 }
			 //print_r($this->arrPrecos);
			 
			 //echo count($this->arrPrecos[5]);
		}
		
		function contaGanhos($idC){
		    $this->arrPrecos = ""; //zerando o array
		    $conecta = $this->construct();
			
			//pegando as respostas das cota��es
			 $resp1 = $conecta->getConsulta2("SELECT * FROM `respitemcotacao` WHERE idCotacao={$idC} GROUP BY `idItemCotacao`");
			 
			 $ar = array();
			 //$arIds = array();
			 
			 while ($row1=$resp1->fetch_assoc()){			 	   
				   $ar = "";			       
				   $idItem = $row1['idItemCotacao'];
				   //pegando os menores e maiores valores para o item
			       $r = $conecta->getConsulta2("SELECT *, min(preco) as 'menor', max(preco) as 'maior' FROM `respitemcotacao` WHERE (`idItemCotacao`={$idItem})");
				   //echo "SELECT *, min(preco) as 'menor', max(preco) as 'maior' FROM `respitemcotacao` WHERE (`idItemCotacao`={$idItem})";
				   $rw = $r->fetch_assoc();
				   //pegando os ids das respostas ganhadoras por item
				   $this->idMim($idItem, $rw['menor'], $rw['maior']);				   
			 }
			 //print_r($this->arrPrecos);
			 
			 //echo count($this->arrPrecos[5]);
		}
		
		function contaPrecosx($idC){
		    $this->arrPrecos = ""; //zerando o array			
			$conecta = $this->construct();
			
			//pegando os fornecedores que enviaram cota��o
			 $resp1 = $conecta->getConsulta2("SELECT * FROM `respitemcotacao` WHERE idCotacao={$idC}");
			 
			 $ar = array(); 
			 
			 while ($row1 = $resp1->fetch_assoc()){
			      
				      $ar = "";
					  $idItem = $row1['idItemCotacao'];
					  
					  $r = $conecta->getConsulta2("SELECT *, min(preco) as 'menor', max(preco) as 'maior' FROM `respitemcotacao` WHERE (`idItemCotacao`={$idItem})");
					  $rrow = $r->fetch_assoc();
					  $menor = $rrow['menor'];
					  $maior = $rrow['maior'];
					  
					  
					  //j� tenho o valor m�nomo, agora, vamos encontra o id do produto do valor m�nimo
					  $respM = $conecta->getConsulta2("SELECT * FROM `respitemcotacao` WHERE (`idItemCotacao`={$idItem} and `preco`={$menor})");
					  while ($rowM = $respM->fetch_assoc()){
					     
						    $ar = "";
							$ar['menor'] = $menor;
					  		$ar['maior'] = $maior;
					  		$idFornecedor = $rowM['idFornecedor'];
							$ar['id'] = $rowM['idRespCotacao'];
							$this->arrPrecos[$idFornecedor] = array($ar);
						  
					  }
					  
					  return $this->arrPrecos;
					  
				  //&*&&*&*&&*&&*continuar a busca nova pelo iditem  e valor minimo
			 }
			 
			 /*
			 while ($row1 = $resp1->fetch_assoc()){
			       $ar = ""; //limpando o array
			       $menor = $row1['menor'];
				   $maior = $row1['maior'];				    
				   $id = $row1['idRespCotacao'];
				   				   
				   $ar['menor'] = $menor;
				   $ar['maior'] = $maior;
				   
			       $re = $conecta->getConsulta2("SELECT * FROM `respitemcotacao` WHERE (`idItemCotacao`={$id} and `preco` = {$menor})");

				   //listando os menores pre�os por fornecedor
				   while ($rr = $re->fetch_assoc()){
				      $ar['id'] = $rr['idRespCotacao'];
				      $idFornecedor = $rr['idFornecedor'];
					  $this->arrPrecos[$idFornecedor] =array($ar);					  
				   }
			 }*/
			 
			 return $this->arrPrecos;
		}	
		
		function contaPrecos($idC){
		    $this->arrPrecos = ""; //zerando o array			
			$conecta = $this->construct();
			
			//pegando os fornecedores que enviaram cota��o
			 $resp1 = $conecta->getConsulta2("SELECT *, (select min(preco) from respitemcotacao where idCotacao={$idC}) as 'menor', (select max(preco) from respitemcotacao where idCotacao={$idC}) as 'maior' FROM `respitemcotacao` WHERE idCotacao={$idC}");
			 //echo "SELECT *, (select min(preco) from respitemcotacao where idCotacao={$idC}) as 'menor', (select max(preco) from respitemcotacao where idCotacao={$idC}) as 'maior' FROM `respitemcotacao` WHERE idCotacao={$idC}";
			 $ar = array(); 
			 
			 while ($row1 = $resp1->fetch_assoc()){
			       $ar = ""; //limpando o array
			       $menor = $row1['menor'];
				   $maior = $row1['maior'];				    
				   $id = $row1['idRespCotacao'];
				   				   
				   $ar['menor'] = $menor;
				   $ar['maior'] = $maior;
				   
			       $re = $conecta->getConsulta2("SELECT * FROM `respitemcotacao` WHERE (`idItemCotacao`={$id} and `preco` = {$menor})");

				   //listando os menores pre�os por fornecedor
				   while ($rr = $re->fetch_assoc()){
				      $ar['id'] = $rr['idRespCotacao'];
				      $idFornecedor = $rr['idFornecedor'];
					  $this->arrPrecos[$idFornecedor] =array($ar);					  
				   }
			 }
			 
			 return $this->arrPrecos;
		}
		
		
		function contaMenoresPrecos($idC){
		    //buscando todos os produtos com pre�os lan�ados, marcando o id do fornecedor ganhador do menor pr�o
			$this->arContaMenores = "";
			$conecta = $this->construct();	

			 //pegando os fornecedores que enviaram cota��o
			 $resp1 = $conecta->getConsulta2("Select * from respitemcotacao group by idFornecedor");
			 $ar = array(); 
			 while ($row1 = $resp1->fetch_assoc()){
			      $idFornecedor = $row1['idFornecedor'];
			      $resp = $conecta->getConsulta2("Select *, min(preco) as 'menor' from respitemcotacao where (idCotacao={$idC}) group by idItemCotacao");
				  $ar = '';
				  while ($row = $resp->fetch_assoc()){
				       if($row['idFornecedor'] == $idFornecedor)
			             $ar[] = $row['idRespCotacao'];
				  }
				  $this->arContaMenores[$idFornecedor] = $ar;
			 }	
			
			return $this->arContaMenores;
		}
		
		function lista($table, $tp){
			$conecta = $this->construct();	
		  if (($table == 'busca')){ 	
			
		    $str = "SELECT produto.*, modelo.modelo  FROM `produto` 
				inner join `modelo` on modelo.idModelo = produto.idModelo 
			    WHERE ((modelo.modelo like '%$tp%') and (produto.status = 'Sim')) ORDER BY `idProduto`";
		   }
		   
		   	$resp = $conecta->getConsulta2($str);
			if ($resp->num_rows > 0){
			   return $resp;
			}else{
			  $str2 = "SELECT produto.*, modelo.modelo  FROM `produto` 
				inner join `modelo` on modelo.idModelo = produto.idModelo 
			    WHERE ((produto.status = 'Sim') and (";
				  $quebra = explode(" ",$tp);
				  for ($c=0; $c < count($quebra); $c++){
					$str2.= "(modelo.modelo like '%".$quebra[$c]."%')";
					if ($c < (count($quebra) - 1)){
					  $str2.=" OR ";
					}
				  }
				$str2.=")) ORDER BY `idProduto`";  
				
				$resp2 = $conecta->getConsulta2($str2);
				
				return $resp2;
			}
		}
		
		public function listaMarcas(){
			  $conecta = $this->construct();
			  $str = "SELECT * FROM `marca` ORDER BY `Marca`";
			  return $conecta->getConsulta2($str);
			}
		public function listaCategorias(){
			  $conecta = $this->construct();
			  $str = "SELECT * FROM `categoria` ORDER BY `categoria`";
			  return $conecta->getConsulta2($str);
			}	
			
		public function lista2($i){
		    $conecta = $this->construct();	
			
			
		    $str = "SELECT * FROM  `produto` INNER JOIN categoria ON categoria.idCategoria = produto.idcategoria WHERE produto.status =  'Ativo' ORDER BY `idProduto` DESC LIMIT $i , 8";
			
			return $conecta->getConsulta2($str);		 
		}
		function busca($palavra){
			$conecta = $this->construct();	
			$str = "SELECT *  FROM `produto` 
					INNER JOIN categoria ON produto.categoria_id = categoria.categoria_id
					WHERE ((produto.produto_Nome LIKE '%$palavra%')and (produto.status = 'Sim'))
					LIMIT 0 , 30";
					
		   return $conecta->getConsulta2($str);
		   
		}
		
		public function lista3($i, $idCat, $idMa){
		    $conecta = $this->construct();	
						
		    $str = "SELECT * FROM  `produto` INNER JOIN categoria ON categoria.idCategoria = produto.idcategoria WHERE ((produto.status =  'Ativo') ";
			
			if ($idCat > 0){
			  $str.=" AND (produto.idCategoria = $idCat)";
			}
			if ($idMa > 0){
			  $str.=" AND (produto.idMarca = $idMa)";
			}
			
			$str.=") ORDER BY `idProduto` DESC LIMIT $i , 12";
			
			return $conecta->getConsulta2($str);		 
		}
		
		function consultaeRetorna($id){
			$conecta = $this->construct();
			$str = "SELECT *  FROM `produto` WHERE `produto_Id` = $id";
			$resp1 = $conecta->getConsulta2($str);
			
			$row = $resp1->fetch_assoc();
			$dados['nome'] = $row['produto_Nome'];
			$dados['ano1'] = $row['produto_AnoModelo'];
			$dados['ano2'] = $row['produto_AnoFabricacao'];
			
			//pegando categoria
			$str2 = "SELECT * FROM `categoria` WHERE `categoria_Id` = ". $row['categoria_Id'];
			$resp2 = $conecta->getConsulta2($str2);
			
			$row2 = $resp2->fetch_assoc();
			$dados['categoria'] = $row2['categoria_Nome'];
			
			//tipo
			$str3 = "SELECT * FROM `tipo` WHERE `tipo_Id` = ". $row['tipo_Id'];
			$resp3 = $conecta->getConsulta2($str3);
			
			$row3 = $resp3->fetch_assoc();
			$dados['tipo'] = $row3['tipo_Nome'];
			
			return $dados;
			
		}
		
		public function conta(){
		   $conecta = $this->construct();	
		   
		   $str0 = "SELECT * FROM `produto` WHERE (status = 'Ativo')";
			$resp0 = $conecta->getConsulta2($str0);
			
			$contador = $resp0->num_rows;
			
			return $contador;
		}
		
		
	}
?>