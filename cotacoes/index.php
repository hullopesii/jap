<?php

   session_start("cotacao");
   
   require_once("../classes/Connection.php");
   $query = new Connection();
   
   require_once("../classes/Funcoes.php");
   $funcoes = new Funcoes();
      
   $data = '';
   $nm = '';
   if ($_GET){
			  $for = $_SESSION['idFornecedor'] = $_GET['for'];
			  $id  = $_SESSION['idCotacao'] = $_GET['id'];
			  //echo "SELECT * FROM `cotacoes` WHERE (`idCotacao`={$id} and `status`= 'Ativo')";
			  $resp = $query->getConsulta2("SELECT * FROM `cotacoes` WHERE (`idCotacao`={$id} and `status`= 'Ativa')");
			  
			  if ($resp->num_rows > 0){
				 //cotação correta
				 $row = $resp->fetch_assoc();
				 $data = 'Cotação nº '. $row['idCotacao'];
				 $data .=' de '. $funcoes->trataData($row['data']);
			  }else{
				 header("location:http://www.jataiautopecas.com.br");
			  }
			  
			  //checando se o fornecedor está correto
			  $resp1 = $query->getConsulta2("SELECT * FROM `fornecedores` WHERE (`idFornecedor`={$for})");
			  
			  if ($resp1->num_rows > 0){
					 //fornecedor corret0
					 $row1 = $resp1->fetch_assoc();
					 $nm = $_SESSION['fornecedornm'] = $row1['fornecedor'];	 
			  }else{
				 header("location:http://www.jataiautopecas.com.br");
			  }
		
		$post = $_POST;
		//$u = array();
		  $u = array_keys($post);
		  for ($x=0; $x < count($u); $x++){
			  $key = $u[$x];
			  if ($post[$key] !== ''){
				 $valor = $post[$key];
				 $valor = str_replace('.', '', $valor);
				 $valor = str_replace(',', '.', $valor);
				 //vendo se o item já foi cadastrado
				 if ($funcoes->constataItemInserido($for, $id, $u[$x]) == 0)
				 		$str = "INSERT INTO `respitemcotacaotemp` VALUES ('', ".$u[$x].", {$for}, {$id}, {$valor})";
				 else
				 		$str = "UPDATE  `respitemcotacaotemp` SET  `preco` =  '{$valor}' WHERE  ((`idItemCotacao`={$u[$x]}) and (
			`idFornecedor` = {$for}) and (`idCotacao` = {$id}))";	
				
				 $query->getConsulta2($str);
			  }
			  //echo "código: ". $u[$x]." R$ " .$post[$key]. "<br />"; 
		  }	  
	  
   }//close do get, que pega as variáveis de fornecedor e cotação
   
   //paginação
   //contando os itens
			  $resp20 = $query->getConsulta2("SELECT * FROM `itenscotacoes` WHERE `idCotacao`={$id}");
			  $total = $resp20->num_rows;
			  $TAMANHO_PAGINA = 20;
			  
			  if (empty($_GET['i'])){
					$inicio = 0; 
					$pagina=1;
			  }else{
					$pagina = $_GET['i'];
					
			   } 
				$inicio = ($pagina - 1) * $TAMANHO_PAGINA;												
				$num_total_registos = $total;
				//calculo o total de páginas 
				$total_paginas = ceil($num_total_registos / $TAMANHO_PAGINA); 
             
   
   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cota&ccedil;&atilde;o de pe&ccedil;as/produtos Jata&iacute; Auto Pe&ccedil;as <?php echo $data; ?></title>

<script type="text/javascript" src="../scripts/jquery.1.5.2.min.js"></script>
<script type="text/javascript" src="../scripts/jquery.ui.js"></script>
<script type="text/javascript" src="../scripts/jquery.validate.js"></script>
<script>

  $().ready(function() {
		$("#formulario").validate();
	});
	
   function submita(){
      $("#formulario").submit();
   }	
   function volta(){
   	  location.href = "?i=<?php echo $pagina - 1; ?>&for=<?php echo $for; ?>&id=<?php echo $id; ?>"; 	
   }
	
</script>
<style>
  .error{ color:#CC0000;}
</style>

</head>

<body style="background-color:#CCCCCC">
 <center>
   <div style="width:940px; padding:10px; text-align:justify; background-color:#FFFFFF;">
	<img src="../img/logo.png" />
	
	   <h2><?php if($nm !== '') echo "Seja Bem vindo, {$nm}"; ?></h2>
	   <p>Segue abaixo a lista dos produtos em cotação. Favor preencher os campos e clicar no botão Salvar.<br />Caso não tenha o produto cotado, favor deixar em branco.</p>
	   <?php
	            //criando  o link do action do formulário
				if ($pagina < $total_paginas){
				    $pxp = $pagina+1;
					$action = "?i=$pxp&for={$for}&id={$id}";
				}else{
					//$action = 'salvaCotacao.php';	
					$pxp = $pagina+1;
					$action = "?i=$pxp&for={$for}&id={$id}";				
				}
	    ?>
	 <form method="post" action="<?php echo $action; ?>" id="formulario">  
	   <table width="100%" border="0" cellspacing="5" cellpadding="3">
		  <tr bgcolor="#CCCCCC">
			<td width="13%">Código</td>
			<td width="42%">Descrição</td>
			<td width="9%">Qtd cotada</td>
			<td width="9%">Marca</td>
			<td width="16%">Valor Unitário</td>
		  </tr>
		  <?php
		      
				
				//fazendo a busca fatal, com paginação
				$resp2 = $query->getConsulta2("SELECT * FROM `itenscotacoes` WHERE `idCotacao`={$id} LIMIT {$inicio},{$TAMANHO_PAGINA}");
			    //caso tenha acabado, vai pra salvaCotação
				if ($resp2->num_rows ==0){
					echo "<script>location.href='salvaCotacao.php';</script>";
				}
			  while ($row2 = $resp2->fetch_assoc()){
			      $valor = $funcoes->checkValue($for, $id, $row2['idItem']);
			      echo "
				     <tr>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['codigo']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['descricao']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px; text-align:right'>{$row2['qtdCotada']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'>{$row2['referencia']}</td>
						<td style='border-bottom:#CCCCCC dashed 1px;'><input type='text' class='number' name='{$row2['idItem']}' value='{$valor}' /></td>
					  </tr>
				  ";
			    
			  }
		  ?>
		</table>
		<?php if ($pagina>1){ ?>
	        <input type="button" value="Anterior" title="Voltar para a página anterior" style=" width:200px; height:40px; float:left;" onclick="volta()" /> 	
	     <?php } ?>
		<input type="button" value="Próximo (<?php echo $pagina ." de ".$total_paginas;  ?>)" title="Próximo passo da cotação" style=" width:200px; height:40px; float:right;" onclick="submita()" />
		<div style="clear:both"></div>
		
       </form>
	   													  <div class="passador">
															  <?php //passador de páginas
																   //mostro os diferentes índices das páginas, se é que há várias páginas /
																   /*
																	if ($total_paginas > 1){
																	   echo "Páginas : "; 
																	   for ($i=1;$i<=$total_paginas;$i++){ 
																		  if ($pagina == $i) 
																			 //se mostro o índice da página actual, não coloco link 
																			 echo $pagina; 
																		  else 
																			 //se o índice não corresponde com a página mostrada actualmente, coloco o link para ir a essa página 
																			 echo "<a href='?i=" . $i . "&for={$for}&id={$id}' class='linkPassa'> &nbsp;" . $i . " &nbsp;</a> "; 
																	   }
																	} */
															  ?>
															  </div>
   </div>
 </center>  	
</body>
</html>
