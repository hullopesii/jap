<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Jata&iacute; Auto Pe&ccedil;as -Sobre Jata&iacute;</title>
<link href="supercss.css" rel="stylesheet" type="text/css" />
<?php 
	require_once("classes/Layout.php");
	
	
	$template = new Layout();
	
	echo $template->tag('A empresa');
	
?>

</head>

<body>
  <div id="mask"></div>
  <div class="ceu"></div>
  <div class="faixaPneuFundo"></div>
  <!-- page is beginning here -->
  <div class="grid">
      <div class="fone"><img src="img/phone.png" alt="telefone" title="Loja 1" class="img" /><span>Loja 1 - 64. 3631-2676</span>
	   <img src="img/phone.png" alt="telefone" title="Loja 2" class="img" /><span>Loja 2 - 64. 3631-2058</span>
	   </div>
	   <a href="/" title="clique para voltar � home"><div class="logo"></div></a>
	   
	   <div class="topo"><img src="img/fotoTopo1.png" /></div>
	   
	   <div class="biela"></div>
	   
	   <div class="faixaPneuMenu">
			<?php //menu
			   echo $template->menu('A empresa');
			?>
	   </div>
	   <div class="content">
	     <!-- beginning the content's site -->
		   <div id="colEsq">
		    <img src="img/fotosPorta.jpg" alt="fotos da jata&iacute; auto pe&ccedil;as" />
			 
			 <h2>Pe�as automotivas para as marcas</h2>
			 <img src="img/marcasPecas.jpg" alt="marcas que atendemos: audi, chevrolet, citroen, fiat, ford, gm, nissan, mitsubishi, peugeot, renaut, toyota, volare, volkswagen" />	 
			    
			 
		   </div>
		   <div id="colDir"> 		
		     <h2>Hist�ria de Nossa Cidade</h2>
			  <p>A cidade de Jata� constitui-se na �ltima fase da expans�o do gado que, vindo da Zona Leste do Brasil, atrav�s do rio S�o Francisco, tomou conta de Minas Gerais e veio at� Goi�s e Mato Grosso. At� essa �poca, a regi�o sudoeste era pouco conhecida, envolta em perigos e mist�rios. Mas, sustentados pelo sonho desenvolvimentista, os primeiros desbravadores chegaram � regi�o, ignorando quaisquer obst�culos.</p><p>
Em setembro de 1836, Francisco Joaquim Vilela e seu filho Jos� Manoel Vilela, procedentes de Esp�rito Santo dos Coqueiros, munic�pio de Lavras do Funil, hoje cidade de Coqueiral, Estado de Minas Gerais, entraram pelo leste, atrav�s de Rio Verde nos sert�es do sudoeste goiano, onde montaram uma fazenda de cria��o de gado, �s margens dos Rios Claro e Ariranha.</p><p>
Do encontro dos dois pioneiros ficou acertado amigavelmente, de modo definitivo, simples e pr�tico, que as terras banhadas por �guas da margem esquerda do Ariranha, pertenceriam aos Vilelas, e as percorridas por afluentes do Bom Jardim, seriam dos Carvalhos.
Em 1837, o jovem Jos� de Carvalho Bastos, proveniente de Franca, S�o Paulo, acompanhado de sua esposa Ana C�ndida Gouveia de Moraes, chegou � regi�o atrav�s de Santana do Parana�ba, em busca de boas terras goianas e se instalou �s margens do Ribeir�o Bom Jardim.
Posteriormente formou-se ent�o o primeiro n�cleo de povoa��o, com terreno doado por Francisco Joaquim Vilela e sua mulher Genoveva Maximina Vilela, recebendo o nome de Para�so.</p><p>
Em 17 de agosto de 1864, o Presidente da Prov�ncia de Goi�s elevou a categoria de Freguesia, a Capela do Divino Esp�rito Santo de Jata�, criando assim o Distrito de Para�so de Jata�.</p><p>
Em 09 de julho de 1867, foi lan�ada a pedra fundamental da Igreja, pelo padre Ant�nio Marques Santar�m.</p><p>
Em 28 de julho de 1882 de acordo com a resolu��o n� 668 foi lan�ada a pedra fundamental, criando o munic�pio de Para�so.</p><p>
Em 02 de fevereiro de 1885 recebeu o nome de Jata�. No entanto, foi atrav�s da Lei Estadual n� 56 de 31 de maio de 1895, que a sede do munic�pio se elevou � categoria de cidade de Jata�, por imposi��o do Tenente Coronel Jos� Manoel Vilela. A comarca de Jata� foi implantada em 21 de julho de 1898 desmembrando-se judicialmente de Rio Verde.</p>
			  

	       </div> 
		   <div class="empurra"></div>
		 <!-- //content-->
	   </div>
	   <?php //rodape
	      echo $template->rodape();
	   ?>
  </div>
  <!-- page is over here -->
</body>
</html>